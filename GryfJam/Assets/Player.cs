﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Rigidbody _body;
    [SerializeField] private float _speed;
    private float _horizontal, _vertical;
    [SerializeField] private int _maxHp;
    private int _currentHp;
    [SerializeField] private GameObject _projectile;

    private void Awake()
    {
        _currentHp = _maxHp;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
            Shot();

        _horizontal = Input.GetAxis("Horizontal");
        _vertical = Input.GetAxis("Vertical");
    }

    private void Shot()
    {
        Instantiate(_projectile);
    }

    private void FixedUpdate()
    {
        Move(_horizontal, _vertical);
    }

    // Call it in fixedUpdate in order to move rigidbody according to axies input
    private void Move(float horizontal, float vertical)
    {
        if (_body != null)
        {
            Vector2 velocity = new Vector2(horizontal, vertical).normalized * _speed;
            _body.velocity = velocity;
        }
    }

    public void TakeDamage(int dmg)
    {
        _currentHp = Mathf.Clamp(_currentHp - dmg, 0, _maxHp);
        if (_currentHp == 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBehavior : MonoBehaviour {

    protected Spell _spell;

    public void Init(Spell spell)
    {
        _spell = spell;
    }

    public virtual void OnCast()
    {
        GetComponentInChildren<Rigidbody2D>().velocity = _spell.Direction * _spell.Speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collision spell-"+ collision.collider.name);
        StatComponent hp = collision.collider.GetComponentInParent<StatComponent>();
        if (hp != null && hp.stat.Name == "Health")
        {
            hp.stat.Value -= _spell.Power;
        }
    }

}

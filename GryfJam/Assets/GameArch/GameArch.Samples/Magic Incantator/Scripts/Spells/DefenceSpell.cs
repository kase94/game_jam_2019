﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceSpell : SpellBehavior
{
    [SerializeField] OrbitTarget _orbitTarget;

    public override void OnCast()
    {
        transform.position += _spell.Direction * _orbitTarget.Radius;
        _orbitTarget.Init(_spell.Speed, _orbitTarget.Radius, _spell.Caster.transform);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSpell : SpellBehavior
{
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private float _range = 10;
    private Vector3 _startPos;
    private Vector3 _endPos;
    private Vector3 _laserEdge;
    private bool _miss = false;
    private bool _finished = false;
    private ITarget _target;
    private Vector3 _direction;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_startPos == _spell.CastTransform.position)
        {
            _startPos = _spell.CastTransform.position;
            _direction = _startPos.CalculateMouseDirection(_target.Target);
            _endPos = _startPos + _direction * _range;
            _lineRenderer.positionCount = 2;

            RaycastHit2D hit = Physics2D.Raycast(_startPos, _startPos.CalculateDirection(_endPos), _range, 0 +( ~(1 << _spell.Caster.layer )));
            Vector3 hitPoint = _startPos + _startPos.CalculateDirection(_endPos) * _range;

            if (hit.collider != null)
            {
                hitPoint = hit.point;
                _miss = false;
            }
            else
            {
                _miss = true;
            }

            if (_miss == true && _finished == true)
                _laserEdge = Vector3.Lerp(_laserEdge, _startPos, _spell.Speed);
            else
                _laserEdge = Vector3.Lerp(_laserEdge, hitPoint, _spell.Speed);

            Debug.DrawLine(_startPos, _laserEdge);
            _lineRenderer.SetPosition(0, _startPos);
            _lineRenderer.SetPosition(1, _laserEdge);

            if (_finished == false && Vector3.Distance(_laserEdge, hitPoint) <= 0.1f)
            {
                _finished = true;
            }

            if (_finished == true && Vector3.Distance(_laserEdge, _startPos) <= 0.1f)
            {
                Destroy(gameObject);
            }

            if (_finished == true && _miss == false)
            {
                Vector3 targetPos = hit.collider.gameObject.transform.position;
                hit.collider.gameObject.transform.position = Vector3.MoveTowards(targetPos, _startPos, _spell.Speed);
            }

        }
        else
        {
            Destroy(gameObject);
        }

    }

    public override void OnCast()
    {
        _startPos = _spell.CastTransform.position;
        _endPos = _startPos + _spell.Direction * _range;
        _laserEdge = _startPos;
        _target = _spell.Caster.GetComponent<ITarget>();
    }

}

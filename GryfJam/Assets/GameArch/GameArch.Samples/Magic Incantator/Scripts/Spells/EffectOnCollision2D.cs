﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectOnCollision2D : MonoBehaviour {

    public GameObject EffectPrefab;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var effect = Instantiate(EffectPrefab);
        effect.transform.position = gameObject.transform.position;
        Destroy(transform.root.gameObject);
    }
}

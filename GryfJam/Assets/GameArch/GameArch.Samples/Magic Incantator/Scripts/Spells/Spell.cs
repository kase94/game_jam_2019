﻿using UnityEngine;

[CreateAssetMenu(menuName = "Spells/Create new spell")]
public class Spell : ScriptableObject
{
    public string Name;
    public Sprite Icon;
    public int ManaCost;
    public int Power;
    public float Speed;
    [SerializeField] protected GameObject _prefab;
    protected SpellBehavior _behavior;

    public GameObject Caster { get; private set; }
    public Transform CastTransform { get; private set; }
    public Vector3 Direction { get; private set; }

    public virtual void Cast(GameObject caster, Transform castTransform, Vector3 direction)
    {
        Caster = caster;
        CastTransform = castTransform;
        Direction = direction;

        GameObject go = Instantiate(_prefab, castTransform.position, Quaternion.identity);
        go.ChangeLayer(caster.layer);
        _behavior = go.GetComponent<SpellBehavior>();
        _behavior.Init(this);
        _behavior.OnCast();
    }
}

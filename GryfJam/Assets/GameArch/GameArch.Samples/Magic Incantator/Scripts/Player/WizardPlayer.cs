﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameArch.Systems.Input;
using static GameArch.Systems.Input.InputManager;
using GameArch.Helpers;

public class WizardPlayer : MonoBehaviour {

    public Spell AttackSpell;
    public Spell DefSpell;
    public Spell CollectSpell;

    public RangedAttack rangedAttack;

    [SerializeField]
    private ManaPool _manaPool;

    private bool _canCast = true;
    private List<InputSnapShot> _lastInput = null;
    InputManager _input;
    [SerializeField]
    private Timer _castCooldown;

    // Use this for initialization
    void Awake ()
    {
        /*
        var inputButtons = new List<string>()
        {
            "Fire1", "Fire2", "Fire3" , "Jump"
        };

        InputConfig config = new InputConfig(buttons:inputButtons);
        _input.Init(config, true);
        _input.StartRecord();
        */
        _input = InputManager.Instance;
    }

    // Update is called once per frame
    void Update ()
    {
    }

    private void FixedUpdate()
    {
        if(_canCast == false)
        {
            _canCast = _castCooldown.Run(Time.fixedDeltaTime);
        }
        var input = _input.GetLastInput();
        _lastInput = input.GetSnapShots();
        foreach (var snapShot in _lastInput)
        {
            if (snapShot.input.GetButtonDown("Fire1") && _canCast)
            {
                CastSpell(AttackSpell, snapShot.input.MousePosition);
                _canCast = false;
            }

            if (snapShot.input.GetButtonDown("Fire2") && _canCast)
            {
                CastSpellOnYourself(DefSpell, snapShot.input.MousePosition);
                _canCast = false;
            }

            if (snapShot.input.GetButtonDown("Jump"))
            {
                CastSpell(CollectSpell, snapShot.input.MousePosition);
                // _input.StartReplay();
                // break;
                _canCast = false;
            }
        }

    }

    private void CastSpell(Spell spell, Vector3 mousePosition)
    {
        if (spell.ManaCost <= _manaPool.Mana)
        {
            Instantiate(spell).Cast(gameObject, rangedAttack.ShotPos, transform.position.CalculateMouseDirection(mousePosition));
            _manaPool.Mana -= spell.ManaCost;         
        }
    }

    private void CastSpellOnYourself(Spell spell, Vector3 mousePosition)
    {
        if (spell.ManaCost <= _manaPool.Mana)
        {
            Instantiate(spell).Cast(gameObject, transform, transform.position.CalculateMouseDirection(mousePosition));
            _manaPool.Mana -= spell.ManaCost;
        }
    }
}

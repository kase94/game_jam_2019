﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPool : MonoBehaviour
{
    [SerializeField]
    Stat _baseMana = new Stat();
    [SerializeField]
    Stat _extendedMana = new Stat();
    [SerializeField]
    private float _manaRegenSpeed = 1f;
    private float _manaRegenTimer = 0;

    
    public int Mana
    {
        get
        {
            return _baseMana.Value + _extendedMana.Value;
        }

        set
        {
            if (value > _baseMana.Value + _extendedMana.Value)
            {
                int diff = value - _baseMana.Value - _extendedMana.Value;
                _extendedMana.Value += diff;
                _baseMana.Value = value - _extendedMana.Value;
            }
            else if (value < _baseMana.Value + _extendedMana.Value)
            {
                int diff = _baseMana.Value + _extendedMana.Value - value;
                _baseMana.Value -= diff;
                _extendedMana.Value = value -_baseMana.Value;
            }
        }
    }

    public Stat BaseMana { get { return _baseMana; } private set { } }
    public Stat ExtendedMana { get { return _extendedMana; } private set { } }

    public float ManaRegenTimer
    {
        get
        {
            return _manaRegenTimer;
        }

        private set
        {
            _manaRegenTimer = value;
        }
    }

    public float ManaRegenSpeed
    {
        get
        {
            return _manaRegenSpeed;
        }

        private set
        {
            _manaRegenSpeed = value;
        }
    }

    
    private void FixedUpdate()
    {
        _manaRegenTimer += Time.fixedDeltaTime;
        if(_manaRegenTimer >= _manaRegenSpeed)
        {
            _manaRegenTimer -= _manaRegenSpeed;
            _baseMana.Value += 1;
        }
    }
}

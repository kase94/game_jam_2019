﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownAIChargeMovement : MonoBehaviour {
    internal bool Hold = false;
    [SerializeField] TopDownController _controller;
    [SerializeField] float _followPrecision = 3f;
    Vector3 _playerPos;
    Vector3 _input;
    // Update is called once per frame
    void FixedUpdate()
    {
        FollowTarget();
    }

    void FollowTarget()
    {
        _playerPos = MIGameManager.Instance._player.transform.position;
        if (Vector3.Distance(_playerPos, transform.position) > _followPrecision)
        {
           // _controller.Move(_playerPos);
            _controller.Move(_playerPos);
        }
        else
        {
            _controller.Stop();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    [SerializeField] StatComponent hp;
    private void Awake()
    {
        hp.stat.OnValueChange.AddListener(OnHpChange);
    }

    private void OnHpChange(int oldHp, int newHp)
    {
       if(newHp == 0)
        {
            Destroy(gameObject);
        }
    }
}

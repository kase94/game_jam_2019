﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveAllPhysics : MonoBehaviour {

    [SerializeField] Collider2D _ignorecollider;
    Collider2D _collider;
    Rigidbody2D _body;
    private void Awake()
    {
        _body = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        Physics2D.IgnoreCollision(_collider, _ignorecollider);
    }

    private void FixedUpdate()
    {
        _body.velocity = Vector2.zero;
        _body.angularVelocity = 0;
    }

    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name + " enter");
        _body = collision.gameObject.GetComponent<TopDownAIChargeMovement>();
        if (_body != null)
        {
            _body.Hold = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

  
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name + " exit");
        _body = collision.gameObject.GetComponent<TopDownAIChargeMovement>();
        if (_body != null)
        {
            _body.Hold = false;
        }
    }
    */
}

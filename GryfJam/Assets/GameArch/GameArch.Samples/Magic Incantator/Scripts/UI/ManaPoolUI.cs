﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaPoolUI : MonoBehaviour {

    [SerializeField] GameObject _baseMana, _extendedMana;
    [SerializeField] Text _manaCounterText;
    [SerializeField] GameObject _manaPointPrefab;
    ManaPool _manaPool;
    ManaPointUI _nextManaPoint;
	// Use this for initialization
	void Awake ()
    {
        _manaPool = MIGameManager.Instance._player.GetComponentInChildren<ManaPool>();
        _manaCounterText.text = _manaPool.Mana.ToString();
        SetMana(_baseMana, _manaPool.BaseMana.Value, _manaPool.BaseMana.MaxValue);
        SetMana(_extendedMana, _manaPool.ExtendedMana.Value, _manaPool.ExtendedMana.MaxValue);
        _manaPool.BaseMana.OnValueChange.AddListener(OnBaseManaValueChange);
        _manaPool.BaseMana.OnMaxValueChange.AddListener(OnBaseManaValueChange);
        _manaPool.ExtendedMana.OnValueChange.AddListener(OnExtendedManaValueChange);
        _manaPool.ExtendedMana.OnMaxValueChange.AddListener(OnExtendedManaValueChange);
    }

    private void OnExtendedManaValueChange(int oldValue, int newValue)
    {
        SetMana(_extendedMana, _manaPool.ExtendedMana.Value, _manaPool.ExtendedMana.MaxValue);
        _manaCounterText.text = _manaPool.Mana.ToString();
    }

    private void OnBaseManaValueChange(int oldValue, int newValue)
    {
        SetMana(_baseMana, _manaPool.BaseMana.Value, _manaPool.BaseMana.MaxValue);
        _manaCounterText.text = _manaPool.Mana.ToString();
        // get reference for next mana piont which will be regenerating
        if (_manaPool.BaseMana.Value < _manaPool.BaseMana.MaxValue)
        {
            _nextManaPoint = _baseMana.transform.GetChild(newValue).gameObject.GetComponent<ManaPointUI>();
        }
    }

    private void SetMana(GameObject bar, int min, int max)
    {
        // Destroy all mana points
        for (int i = 0; i < bar.transform.childCount; i++)
        {
            Destroy(bar.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < max; i++)
        {
            GameObject manaPointGO = Instantiate(_manaPointPrefab, bar.transform);
            manaPointGO.GetComponent<ManaPointUI>().SetFill(i < min ? 1 : 0);     
        }
    }

    private void FixedUpdate()
    {
        if (_manaPool.BaseMana.Value < _manaPool.BaseMana.MaxValue)
        {
            _baseMana.transform.GetChild(_manaPool.BaseMana.Value).gameObject.GetComponent<ManaPointUI>().SetFill(_manaPool.ManaRegenTimer / _manaPool.ManaRegenSpeed);
        }
            
    }

    private void OnDestroy()
    {
        _manaPool.BaseMana.OnValueChange.RemoveListener(OnBaseManaValueChange);
        _manaPool.BaseMana.OnMaxValueChange.RemoveListener(OnBaseManaValueChange);
        _manaPool.ExtendedMana.OnValueChange.RemoveListener(OnExtendedManaValueChange);
        _manaPool.ExtendedMana.OnMaxValueChange.RemoveListener(OnExtendedManaValueChange);
    }
}

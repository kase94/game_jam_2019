﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaPointUI : MonoBehaviour {

    [SerializeField] Image _manaFilled;
    
    public void SetFill(float fill)
    {
        _manaFilled.fillAmount = fill;
    }
}

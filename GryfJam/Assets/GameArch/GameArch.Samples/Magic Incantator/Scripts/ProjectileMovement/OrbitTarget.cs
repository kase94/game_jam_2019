﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitTarget : MonoBehaviour
{

    public float Radius
    {
        get
        {
            return _radius;
        }
        set
        {
            _radius = value;
        }
    }
    public float Speed
    {
        get
        {
            return _speed;
        }
        set
        {
            _speed = value;
        }
    }
    public Transform Target { get; set; }

    [SerializeField] private float _radius;
    [SerializeField] private float _speed;
    private Vector3 _lastPos;

    public void Init(float speed, float radius, Transform target)
    {
        Speed = speed;
        Radius = radius;
        Target = target;
        relativeDistance = transform.position - Target.position;
    }

    public Vector3 relativeDistance = Vector3.zero;
    public bool once = true;
    // Use this for initialization
    void Start()
    {
        /*
        if (Target != null)
        {
            relativeDistance = transform.position - Target.position;
        }
        */
    }

    void Orbit()
    {
        if (Target != null)
        {
            // Keep us at the last known relative position
            transform.position = (Target.position + relativeDistance);
            transform.RotateAround(Target.position, Vector3.back, Speed * Time.fixedDeltaTime);
            // Reset relative position after rotate
            if (once)
            {
                transform.position *= Radius;
                once = false;
            }
            relativeDistance = transform.position - Target.position;
        }
    }

    void FixedUpdate()
    {

        Orbit();

    }
}

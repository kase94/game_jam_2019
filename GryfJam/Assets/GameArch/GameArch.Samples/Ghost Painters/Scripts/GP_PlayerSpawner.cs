﻿using GameArch.Systems;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Samples.GhostPainters
{
    public class GP_PlayerSpawner : MonoBehaviour
    {
        [SerializeField] Transform[] _spawnPoints;
        [SerializeField] GameObject _playerPrefab;

        void Awake()
        {
            if(GP_GameManager.Instance.PlayerControls != null)
                SpawnPlayers(GP_GameManager.Instance.PlayerControls);
        }

        private void SpawnPlayers(List<string> playerControls)
        {
            if (playerControls.Count <= _spawnPoints.Length)
            {
                var uniquePos = MathHelper.GetUniqueRandomArray(0, _spawnPoints.Length - 1, playerControls.Count);

                for (int i = 0; i < playerControls.Count; i++)
                {
                    var playerObj = Instantiate(_playerPrefab, _spawnPoints[uniquePos[i]].position, _playerPrefab.transform.rotation);
                    playerObj.GetComponent<GP_Player>().Init(playerControls[i]);
                }
            }
            else
            {
                Debug.Log($"GP_PlayerSpawner error: not enough spawn points for {playerControls.Count} players");
            }

        }
    }
}
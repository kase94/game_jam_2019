﻿using GameArch.Systems;
using GameArch.Systems.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameArch.Samples.GhostPainters
{
    public class GP_GameManager : Singleton<GP_GameManager>
    {
        [SerializeField] SceneReference _gameScene;

        List<string> _playerControls;

        public List<string> PlayerControls { get => _playerControls; private set => _playerControls = value; }

        private void Start()
        {
            EventManager.Instance.AddListener(CoopPlayerLobby.GetIdentity(), CoopPlayerLobby.Events.GAME_START.name, OnGameStart);
        }

        private void OnGameStart(ActionParams actionParams)
        {
         //   PlayerControls = CoopPlayerLobby.Events.GAME_START.GetData(actionParams);
            SceneManager.LoadScene(_gameScene.SceneName);
        }

        private void OnDestroy()
        {
            EventManager.Instance.StopListening(CoopPlayerLobby.GetIdentity(), CoopPlayerLobby.Events.GAME_START.name, OnGameStart);
        }


        #region EventManager

        public static Identity GetIdentity()
        {
            return Identity.By.Name(typeof(GP_GameManager).ToString());
        }

        public static class Events
        {
            /*
            public static class GAME_START
            {
                public const string name = "Game Start";

                internal static ActionParams CreateData(List<string> registeredPlayersControls)
                {
                    ActionParams playersParams = new ActionParams();
                    playersParams.Add<List<string>>("players", registeredPlayersControls);
                    return playersParams;
                }

                public static List<string> GetData(ActionParams playersParams)
                {
                    return playersParams.As<List<string>>("players");
                }
            }
            */
        }

        #endregion

    }
}

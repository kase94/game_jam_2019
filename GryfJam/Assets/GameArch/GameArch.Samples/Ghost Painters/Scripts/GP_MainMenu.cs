﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameArch.Samples.GhostPainters
{
    public class GP_MainMenu : MonoBehaviour
    {
        [SerializeField] string _playSceneName;

        public void OnPlay()
        {
            SceneManager.LoadScene(_playSceneName);
        }

        public void OnExit()
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
    }
}
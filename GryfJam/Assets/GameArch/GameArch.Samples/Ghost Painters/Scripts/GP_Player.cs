﻿using GameArch.Systems.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Samples.GhostPainters
{
    public class GP_Player : MonoBehaviour
    {
        [SerializeField] TopDownPlayerMovement _movement;

        public void Init(Controls controls)
        {
            _movement.Controls = controls;
        }

        public void Init(string controls)
        {
            _movement.Controls = InputControl.GetControlsByName(controls);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameArch.Samples.SeedGame
{
    public class Seed_Round : MonoBehaviour
    {
        [SerializeField] HeadData _headData;
        [SerializeField] Seed_PlayerScore[] _players;
        [SerializeField] Text _round;

        public void Init(string winner)
        {
            foreach (var item in _players)
            {
                item.gameObject.SetActive(false);
            }

           _players[0].Image.sprite = _headData._headsData[Seed_GameManager.Instance.PlayerData[winner]].normal;
           _players[0].Text.text = Seed_GameManager.Instance.PlayerScore[winner].ToString();
           _players[0].gameObject.SetActive(true);

            int i = 1;
            foreach (var player in Seed_GameManager.Instance.PlayerData)
            {
                if (player.Key != winner)
                {
                    _players[i].gameObject.SetActive(true);
                    _players[i].Image.sprite = _headData._headsData[Seed_GameManager.Instance.PlayerData[player.Key]].hit;
                    _players[i].Text.text = Seed_GameManager.Instance.PlayerScore[player.Key].ToString();
                }
                else
                {
                    continue;
                }
                i++;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Samples.SeedGame
{
    public class FootCollider : MonoBehaviour
    {
        public bool _isSmashing = false;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Player" && _isSmashing == true)
            {
                var player = collision.gameObject.GetComponentInParent<Seed_Player>();
                player.Death(5);
            }
        }
    }
}

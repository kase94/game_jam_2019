﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Samples.SeedGame
{
    public class Seed_Gum : MonoBehaviour
    {
        public Animator _strechAnimator;
        public Animator _slowAnimator;
        public float _lifeTime = 10f;

        public void StartStretchAnimation()
        {
            _strechAnimator.enabled = true;
            Destroy(gameObject, _lifeTime);
        }

        public void StartSlowAnimation()
        {
            _slowAnimator.enabled = true;
        }


        [Range(0, 1)]
        public float speedReductionFactor = .5f;

        List<GameObject> objectsInside;

        Rigidbody2D rb;

        void Start()
        {
            objectsInside = new List<GameObject>();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
           var player =  other.gameObject.GetComponentInParent<Seed_Player>();
            if(player)
                player.ChangeSlowFactor(speedReductionFactor);
            objectsInside.Add(other.gameObject);
        }

        void OnTriggerExit2D(Collider2D other)
        {
            var player = other.gameObject.GetComponentInParent<Seed_Player>();
            if (player != null && objectsInside.Contains(other.gameObject))
            {
                player.ChangeSlowFactor(1);
                objectsInside.Remove(other.gameObject);
            }
        }

    }

}
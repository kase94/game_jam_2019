﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace GameArch.Samples.SeedGame
{
    public class Seed_EndScreen : MonoBehaviour
    {
        [SerializeField] Image _winner;
        [SerializeField] Image[] _looser;
        [SerializeField] HeadData _headData;

        public void Init(string winner)
        {
            foreach (var img in _looser)
            {
                img.enabled = false;
            }

            _winner.sprite = _headData._headsData[Seed_GameManager.Instance.PlayerData[winner]].normal;

            int i = 0;
            foreach (var player in Seed_GameManager.Instance.PlayerData)
            {
                if (player.Key != winner)
                {
                    _looser[i].enabled = true;
                    _looser[i].sprite = _headData._headsData[Seed_GameManager.Instance.PlayerData[player.Key]].hit;
                }
                else
                {
                    continue;
                }
                i++;
            }
        }
    }
}
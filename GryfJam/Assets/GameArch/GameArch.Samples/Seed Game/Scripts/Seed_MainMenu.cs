﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameArch.Samples.SeedGame
{
    public class Seed_MainMenu : MonoBehaviour
    {
        [SerializeField] string _playSceneName;

        public void OnPlay()
        {
            SceneManager.LoadScene(_playSceneName);
        }

        public void OnExit()
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Return))
            {
                OnPlay();
            }
            else if (Input.GetKey(KeyCode.Escape))
            {
                OnExit();
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Samples.SeedGame
{
    public class ObstacleSpawner : MonoBehaviour
    {
        [SerializeField] Transform[] _spawnPoints;
        [SerializeField] GameObject _obstaclePrefab;
        [SerializeField] float _spawnInterval;
        [SerializeField] int _spawnSize;
        [SerializeField] int _spawnerLife = -1;
        float _spawnTimer;

        private void Update()
        {

            _spawnTimer += Time.deltaTime;
            if (_spawnTimer >= _spawnInterval && (_spawnerLife > 0 || _spawnerLife == -1))
            {
                if(_spawnerLife > 0)
                    --_spawnerLife;
                if (_spawnSize <= _spawnPoints.Length)
                {
                    var uniquePos = MathHelper.GetUniqueRandomArray(0, _spawnPoints.Length, _spawnSize);

                    for (int i = 0; i < _spawnSize; i++)
                    {
                        var obstacle = Instantiate(_obstaclePrefab, _spawnPoints[uniquePos[i]].position, _obstaclePrefab.transform.rotation);

                    }
                }
                else
                {
                    Debug.Log($"ObstacleSpawner error: not enough spawn points for spawn size: {_spawnSize}");
                }
                _spawnTimer = 0;
            }
        }
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameArch.Samples.SeedGame
{
    public class FootController : MonoBehaviour
    {
        [SerializeField] Seed_GumSpawner _gumSpawner;
        [SerializeField] FootCollider _footCollider;
        [SerializeField] float _lifeTime = 2f;
        [SerializeField] Collider2D _collider;
        [SerializeField] GameObject _footParticle;
        [SerializeField] Transform _particlePosition;

        // Start is called before the first frame update
        void Start()
        {
            _collider.enabled = false;
            Destroy(gameObject, _lifeTime);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ActivateSmashing()
        {
            _collider.enabled = true;
            _footCollider._isSmashing = true;
        }

        public void DisableSmashing()
        {
            _footCollider._isSmashing = false;
            var particle = Instantiate(_footParticle);
            particle.transform.position = _particlePosition.position;
           
            CameraShake.Instance.Shake();
        }

        public void DisableCollider()
        {
            _gumSpawner.ReleaseGum();
            _collider.enabled = false;
            _gumSpawner.StartAnimation();
        }
    }
}

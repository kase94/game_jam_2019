﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameArch.Samples.SeedGame
{
    public class Seed_GumSpawner : MonoBehaviour
    {
        [SerializeField] Transform[] _spawnPoints;
        [SerializeField] GameObject _obstaclePrefab;
        [SerializeField] int _minSpawnSize;
        [SerializeField] int _maxSpawnSize;
        [SerializeField] float _spawnChance = 20;
        List<GameObject> _spawnedGum = new List<GameObject>();

        private void Start()
        {
            Spawn();
        }

        public void Spawn()
        {
            if (Random.Range(0f, 100f) < _spawnChance)
            {
                var spawnSize = Random.Range(_minSpawnSize, _maxSpawnSize + 1);
                if (spawnSize <= _spawnPoints.Length)
                {
                    var uniquePos = MathHelper.GetUniqueRandomArray(0, _spawnPoints.Length - 1, spawnSize);

                    for (int i = 0; i < spawnSize; i++)
                    {
                        var obstacle = Instantiate(_obstaclePrefab, _spawnPoints[uniquePos[i]].position, _obstaclePrefab.transform.rotation);
                        obstacle.transform.SetParent(_spawnPoints[uniquePos[i]]);
                        _spawnedGum.Add(obstacle);
                    }
                }
                else
                {
                    Debug.Log($"Seed_GumSpawner error: not enough spawn points for spawn size: {spawnSize}");
                }
            }
        }

        public void ReleaseGum()
        {
            for (int i = 0; i < _spawnedGum.Count; i++)
            {
                _spawnedGum[i].transform.parent.DetachChildren();
            }
        }

        public void StartAnimation()
        {
            for (int i = 0; i < _spawnedGum.Count; i++)
            {
                _spawnedGum[i].GetComponent<Seed_Gum>().StartStretchAnimation();
            }
        }
    }
}
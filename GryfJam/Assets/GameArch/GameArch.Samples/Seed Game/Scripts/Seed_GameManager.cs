﻿using GameArch.Systems;
using GameArch.Systems.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameArch.Samples.SeedGame
{
    public class Seed_GameManager : Singleton<Seed_GameManager>
    {
        [SerializeField] SceneReference _gameScene;
        [SerializeField] SceneReference _mainMenu;
        [SerializeField] SceneReference _playerLobby;
        [SerializeField] Seed_EndScreen _winScreen;
        [SerializeField] Seed_Round _roundScreen;
        [SerializeField] AudioSource _audioSource;
        [SerializeField] AudioClip _victory, _start, _death, _main;
        List<string> _playerControls;
        Dictionary<string, int> _playersData;
        Dictionary<string, int> _playersScore;

        public List<string> PlayerControls { get => _playerControls; private set => _playerControls = value; }
        public Dictionary<string, int> PlayerData { get => _playersData; private set => _playersData = value; }
        public Dictionary<string, int> PlayerScore { get => _playersScore; private set => _playersScore = value; }
        private List<string> _activePlayers;

        private void Start()
        {
            EventManager.Instance.AddListener(CoopPlayerLobby.GetIdentity(), CoopPlayerLobby.Events.GAME_START.name, OnGameStart);
            EventManager.Instance.AddListener(Seed_Player.GetIdentity(), Seed_Player.Events.DEATH.name, OnPlayerDeath);
            _audioSource.clip = _main;
            _audioSource.loop = true;
            _audioSource.Play();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if(SceneManager.GetSceneByName(_gameScene.SceneName).isLoaded || (SceneManager.GetSceneByName(_playerLobby.SceneName).isLoaded))
                {
                    ReturnToMainMenu();
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightShift))
            {
                if (_roundScreen.isActiveAndEnabled)
                {
                    NextRound();
                }
                else if (_winScreen.isActiveAndEnabled)
                {
                    PlayAgain();
                }
            }
        }

        private void OnGameStart(ActionParams actionParams)
        { 
            PlayerControls = new List<string>(CoopPlayerLobby.Events.GAME_START.GetData(actionParams).Keys);
            PlayerData = CoopPlayerLobby.Events.GAME_START.GetData(actionParams);
            PlayerScore = new Dictionary<string, int>();
            foreach (var player in PlayerControls)
            {
                PlayerScore.Add(player, 0);
            }
            SceneManager.LoadScene(_gameScene.SceneName);

            _activePlayers = new List<string>(PlayerControls);
            _audioSource.clip = _start;
            _audioSource.loop = true;
            _audioSource.Play();
        }

        private void OnPlayerDeath(ActionParams actionParams)
        {
            string playerName = Seed_Player.Events.DEATH.GetData(actionParams);
            AudioSource.PlayClipAtPoint(_death, Vector3.zero);
            _activePlayers.Remove(playerName);

           
            if (_activePlayers.Count == 1)
            {
                PlayerScore[_activePlayers[0]] += 1;

                if (PlayerScore[_activePlayers[0]] == 3)
                {
                    _winScreen.gameObject.SetActive(true);
                    _winScreen.Init(_activePlayers[0]);
                    _audioSource.clip = _victory;
                    _audioSource.loop = false;
                    _audioSource.Play();
                }
                else
                {
                    _roundScreen.gameObject.SetActive(true);
                    _roundScreen.Init(_activePlayers[0]);
                }
            }
        }

        private void OnDestroy()
        {
            EventManager.Instance.StopListening(CoopPlayerLobby.GetIdentity(), CoopPlayerLobby.Events.GAME_START.name, OnGameStart);
            EventManager.Instance.StopListening(Seed_Player.GetIdentity(), Seed_Player.Events.DEATH.name, OnPlayerDeath);
        }

        public void ReturnToMainMenu()
        {
            _winScreen.gameObject.SetActive(false);
            SceneManager.LoadScene(_mainMenu.SceneName);
            _audioSource.clip = _main;
            _audioSource.loop = true;
            _audioSource.Play();
        }

        public void PlayAgain()
        {
            _winScreen.gameObject.SetActive(false);
            SceneManager.LoadScene(_gameScene.SceneName);
            foreach (var player in PlayerControls)
            {
                PlayerScore.Add(player, 0);
            }
            _activePlayers = new List<string>(PlayerControls);
            _audioSource.clip = _start;
            _audioSource.loop = true;
            _audioSource.Play();
        }

        public void NextRound()
        {
            _roundScreen.gameObject.SetActive(false);
            SceneManager.LoadScene(_gameScene.SceneName);
            _activePlayers = new List<string>(PlayerControls);
        }

        #region EventManager

        public static Identity GetIdentity()
        {
            return Identity.By.Name(typeof(Seed_GameManager).ToString());
        }

        public static class Events
        {
            /*
            public static class GAME_START
            {
                public const string name = "Game Start";

                internal static ActionParams CreateData(List<string> registeredPlayersControls)
                {
                    ActionParams playersParams = new ActionParams();
                    playersParams.Add<List<string>>("players", registeredPlayersControls);
                    return playersParams;
                }

                public static List<string> GetData(ActionParams playersParams)
                {
                    return playersParams.As<List<string>>("players");
                }
            }
            */
        }

        #endregion

    }
}

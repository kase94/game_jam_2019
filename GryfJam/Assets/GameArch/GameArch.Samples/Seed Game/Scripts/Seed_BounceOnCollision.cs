﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Samples.SeedGame
{
    public class Seed_BounceOnCollision : MonoBehaviour
    {
        [SerializeField] float _bounceForce = 5000;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                // calculate force vector
                var force = transform.position - collision.transform.position;
                // normalize force vector to get direction only and trim magnitude
                force.Normalize();
                gameObject.GetComponent<Rigidbody2D>().AddForce(force * _bounceForce);
                collision.gameObject.GetComponent<Seed_Player>().Disableinput();
            }
        }
    }
}
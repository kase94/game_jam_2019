﻿using GameArch.Systems;
using GameArch.Systems.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Samples.SeedGame
{
    public class Seed_Player : MonoBehaviour
    {
        [SerializeField] TopDownPlayerMovement _movement;
        [SerializeField] StatComponent _hpComponent;
        [SerializeField] GameObject _projectile;
        [SerializeField] float _bulletSpeed = 20f;
        [SerializeField] Transform _shotpos;
        [SerializeField] Animator _bodyAnimator;
        [SerializeField] HeadData _headData;
        [SerializeField] SpriteRenderer _head;
        [SerializeField] GameObject _deathParticle;
        [SerializeField] AudioClip _shot;
        [SerializeField] float _shotCooldown = 0.5f;
        float _shotTimer;
        private int _avatarId;
        private bool _isMoving;
        private bool _isDead;

        Controls _controls;
        private void Start()
        {
            if (_controls == null)
                _controls = _movement.Controls;

            _hpComponent.stat.OnValueChange.AddListener(OnHpChange);
            _isDead = false;
        }

        private void OnHpChange(int oldValue, int newValue )
        {
            if (newValue == 0 && _isDead == false)
            {
                EventManager.Instance.TriggerEvent(GetIdentity(), Events.DEATH.name, Events.DEATH.CreateData(_controls.Name), false);
                _isDead = true;
            }
        }

        public void Init(Controls controls)
        {
            _movement.Controls = controls;
            _controls = controls;
        }

        public void Init(string controls, int avatarId)
        {
            _movement.Controls = InputControl.GetControlsByName(controls);
            _controls = InputControl.GetControlsByName(controls);
            _avatarId = avatarId;
            _head.sprite = _headData._headsData[avatarId].normal;
        }

        public void Disableinput()
        {
            _movement.SetInputActive(false);
            StopAllCoroutines();
            StartCoroutine(nameof(DelayedInput));
        }

        IEnumerator DelayedInput()
        {
            yield return new WaitForSeconds(0.3f);
            _movement.SetInputActive(true);
        }

        IEnumerator DelayedHit()
        {
            yield return new WaitForSeconds(0.5f);
            _head.sprite = _headData._headsData[_avatarId].normal;
        }

        public void Death(int dmg = 1)
        {
            _hpComponent.stat.Value -= dmg;

            if (_hpComponent.stat.Value == 0)
            {
                _movement.SetInputActive(false);
                _movement.Stop();
                Destroy(gameObject, 0.5f);
                Instantiate(_deathParticle, transform.position, Quaternion.identity);
            }
            _head.sprite = _headData._headsData[_avatarId].hit;
            StartCoroutine(nameof(DelayedHit));
        }

        public void ChangeSlowFactor(float SlowFactor)
        {
            _movement._controller.SlowFactor = SlowFactor;
        }

        private void Update()
        {
            if (_hpComponent.stat.Value > 0)
            {
                if(_shotTimer > 0)
                    _shotTimer -= Time.deltaTime;

                _bodyAnimator.SetFloat("x", _movement.reallastInput.x);
                _bodyAnimator.SetFloat("y", _movement.reallastInput.y);
                // if (Input.GetButtonDown(_controls.Keys["Fire"].primaryInput.ToString()))
                if (_controls.GetButtonDown("Fire") && _shotTimer <= 0)
                {
                    _shotTimer += _shotCooldown;
                    // Debug.LogWarning("key player:" + _controls.Keys["Fire"].primaryInput.ToString());
                    GameObject bullet = Instantiate(_projectile, transform.position + new Vector3(_movement.lastInput.x, _movement.lastInput.y+0.2f, 0), Quaternion.identity) as GameObject;
                    bullet.GetComponent<Rigidbody2D>().AddForce(_movement.lastInput * _bulletSpeed);
                    AudioSource.PlayClipAtPoint( _shot, transform.position);
                }

                if(_movement.reallastInput.x == 0 && _movement.reallastInput.y == 0)
                {
                    _bodyAnimator.SetBool("isMoving", false);
                }
                else
                {
                        _bodyAnimator.SetBool("isMoving", true);
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.gameObject.tag == "Bullet")
            {
                Death();
            }
        }


        #region EventManager

        public static Identity GetIdentity()
        {
            return Identity.By.Name(typeof(Seed_Player).ToString());
        }

        public static class Events
        {
            public static class DEATH
            {
                public const string name = "DEATH";

                internal static ActionParams CreateData(string playerName)
                {
                    ActionParams playersParams = new ActionParams();
                    playersParams.Add<string>("playerName", playerName);
                    return playersParams;
                }

                public static string GetData(ActionParams playersParams)
                {
                    return playersParams.As<string>("playerName");
                }
            }
        }

        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "HeadData", menuName = "Seed/Heads", order = 1)]
public class HeadData : ScriptableObject
{
    [Serializable]
    public struct Heads
    {
        public Sprite normal;
        public Sprite hit;
    }

    public List<Heads> _headsData;
}

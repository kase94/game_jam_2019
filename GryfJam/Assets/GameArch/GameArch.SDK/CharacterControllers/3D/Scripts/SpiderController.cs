﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpiderController : MonoBehaviour {

    [Serializable]
    public class Leg
    {
        public HingeJoint Top, Bottom;
    }
    public List<Leg> Legs;
    public float TargetPos = 0;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Legs != null && Legs.Count > 0)
        {
            foreach (var leg in Legs)
            {
                SetSpringTargetPosition(ref leg.Top, TargetPos);
                SetSpringTargetPosition(ref leg.Bottom, TargetPos);
            }
        }
	}

    void SetSpringTargetPosition(ref HingeJoint joint, float targetPosition)
    {
        JointSpring sprTemp = joint.spring;
        sprTemp.targetPosition = TargetPos;
        joint.spring = sprTemp;
    }
}

﻿using GameArch.Systems.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.CharacterControllers
{
    public class PlayerMovement2D : MonoBehaviour
    {
        public CharacterController2D controller;

        public float runSpeed = 40f;

        float horizontalMove = 0f;
        bool jump = false;
        bool crouch = false;

        [SerializeField]
        private ControlsConfig _controlsConfig;

        private Controls _controls;

        private void Awake()
        {
            if(_controlsConfig != null)
            {
                _controls = _controlsConfig.Register();
            }
        }

        // Update is called once per frame
        void Update()
        {
            horizontalMove = _controls.GetAxisRaw("Horizontal") * runSpeed;

            if (_controls.GetButtonDown("Up"))
            {
                jump = true;
            }

            if (_controls.GetButtonDown("Down"))
            {
                crouch = true;
            }
            else if (_controls.GetButtonUp("Down"))
            {
                crouch = false;
            }

        }

        void FixedUpdate()
        {
            // Move our character
            controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
            jump = false;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Top Down Character Controller for NavMeshAgent
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public class TopDownCharacterControllerNavMesh : TopDownController
{
    [SerializeField] private GameObject _obstaclePrefab;
    GameObject _obstacleObj;
    private NavMeshAgent _agent;
    private NavMeshObstacle _obstacle;
    Vector3 _lastPos;
    Vector3 _obstacleSize;
    NavMeshSurface sur;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _obstacle = GetComponent<NavMeshObstacle>();
        _agent.updateRotation = false;
        _obstacleSize = _obstacle.size;
        _lastPos = transform.position;

        if(_obstaclePrefab)
            sur = _obstaclePrefab.GetComponent<NavMeshSurface>();
    }

    /// <summary>
    /// Call it in fixedUpdate in order to move according to axies input
    /// </summary>
    /// <param name="horizontal"></param>
    /// <param name="vertical"></param>
    public override void Move(float horizontal, float vertical)
    {
        _obstacle.enabled = false;
        _agent.enabled = true;

        Vector2 velocity = new Vector2(horizontal, vertical).normalized * Speed * Time.fixedDeltaTime;
        _agent.speed = Speed;
        Vector3 offset = new Vector3(velocity.x, velocity.y, velocity.y);
        _agent.destination = transform.position + offset;
        _agent.isStopped = false;
    }

    /// <summary>
    /// Call it in fixedUpdate in order to reach desired position
    /// </summary>
    public override void Move(Vector3 position)
    {
        if (_obstacle.enabled == false)
        {
            _agent.enabled = true;
            _agent.destination = position;
           // _agent.velocity = transform.position.CalculateDirection(_agent.nextPosition).normalized * Speed;

           

        }
        if (_obstacle.enabled == true)
        {
            _obstacle.carving = false;
            _obstacle.enabled = false;
            if (_obstaclePrefab)
            {
                sur.BuildNavMesh();
                //NavMesh.onPreUpdate
                //sur.UpdateNavMesh(sur.navMeshData);
            }
                
            /*
             _agent.radius = 0.5f;
            _agent.velocity = Vector3.zero;
            _agent.updatePosition = true;
            _agent.isStopped = false;
            _agent.Warp(_lastPos);
            */
        }
       
    }

    public override void Stop()
    {

        if (_agent.enabled == false && _obstacle.enabled == false)
        {
            _obstacle.carving = true;
            //_obstacle.radius = 0.5f;
            _obstacle.enabled = true;
        }

        if (_agent.enabled == true)
        {
            //   _agent.velocity = Vector3.zero;
            //   _agent.isStopped = true;
            //   _agent.updatePosition = false;
            //   _lastPos = transform.position;
            //   Debug.Log("stop last pos" + _lastPos);
            //   //_agent.radius = 0;
            _agent.ResetPath();
            _agent.enabled = false;
            //   _obstacle.size = _obstacleSize;
            //   _agent.Warp(_lastPos);
        }


    }
    
    /*
    public override void Stop()
    {
        if (_agent.updatePosition == true)
        {
            _agent.velocity = Vector3.zero;
            _agent.isStopped = true;
            _agent.updatePosition = false;
            _lastPos = transform.position;
            Debug.Log("stop last pos" + _lastPos);
        //    _agent.radius = 0;
           // _agent.Warp(_lastPos);
            _agent.enabled = false;
        }
        if (_obstacleObj == null)
        {
            _obstacleObj = Instantiate(_obstaclePrefab);
            _obstacleObj.transform.position = transform.position;
        }
        
    }*/
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all top down move controllers
/// </summary>
public class TopDownController : MonoBehaviour
{
    public Rigidbody2D Body;
    public float Speed = 20;
    public float SlowFactor = 1f;

    public virtual void Move(float horizontal, float vertical)
    {

    }

    public virtual void Move(Vector3 position)
    {

    }

    public virtual void Stop()
    {

    }
}


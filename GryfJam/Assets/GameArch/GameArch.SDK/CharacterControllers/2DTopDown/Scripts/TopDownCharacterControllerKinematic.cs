﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Top Down Character Controller for kinematic rigidbodies
/// </summary>
public class TopDownCharacterControllerKinematic : TopDownController
{
    [SerializeField]
    private bool _kinematiCollision = true;
    [SerializeField]
    private bool _enableDebug = false;
    private RaycastHit2D[] _contacts = new RaycastHit2D[100];

    /// <summary>
    /// Call it in fixedUpdate in order to move rigidbody according to axies input
    /// </summary>
    /// <param name="horizontal"></param>
    /// <param name="vertical"></param>
    public override void Move(float horizontal, float vertical)
    {
        if (Body != null)
        {
            bool collisionAhead = false;
            Vector2 velocity = new Vector2(horizontal, vertical).normalized * Speed * Time.fixedDeltaTime;
            var direction = velocity.normalized;

            if (_kinematiCollision)
            { 
            // Find contacts along our movement direction.
            var resultCount = Body.Cast(direction, _contacts, velocity.magnitude);

                // We need to find a hit where we're actually moving.
                for (var i = 0; i < resultCount; ++i)
                {
                    var contact = _contacts[i];
                    var distance = contact.distance;

                    // Are we actually moving?
                    if (distance > Mathf.Epsilon)
                    {
                        Body.MovePosition(Body.position + (direction * distance));
                        return;
                    }
                    // If we're moving into a contact then finish as we cannot move.
                    else if (Vector2.Dot(contact.normal, direction) < 0)
                    {
                        DebugLog("---------");
                        DebugLog("direction" + direction);
                        bool diagMove = Mathf.Abs(direction.x) == Mathf.Abs(direction.y);
                        direction += contact.normal;

                        DebugLog("normal" + contact.normal);
                        DebugLog("new direction" + direction);

                        if (diagMove)
                        {
                            if (MathHelper.IsEqual(Mathf.Abs(contact.normal.x), 1f) && MathHelper.IsEqual(contact.normal.y, 0f))
                            {
                                DebugLog("confirmed normal (1,0)");
                                direction.x = 0;// 0.1f * contact.normal.x;
                            }
                            else if (MathHelper.IsEqual(contact.normal.x, 0f) && MathHelper.IsEqual(Mathf.Abs(contact.normal.y), 1f))
                            {
                                DebugLog("confirmed normal (0,1)");
                                direction.y = 0.001f * contact.normal.y;
                            }

                        }
                        DebugLog("move direction" + direction);
                        if (MoveAgain(direction.x, direction.y))
                        {
                            return;
                        }
                        else
                        {
                            collisionAhead = true;
                        }
                    }
                }
            }
            if(collisionAhead == false)
                Body.MovePosition(Body.position + velocity);
        }
    }

    private bool MoveAgain(float horizontal, float vertical)
    {
        if (Body != null)
        {
            Vector2 velocity = new Vector2(horizontal, vertical).normalized * Speed/2 * Time.fixedDeltaTime;
            var direction = velocity.normalized;

            // Find contacts along our movement direction.
            var resultCount = Body.Cast(direction, _contacts, velocity.magnitude);

            // We need to find a hit where we're actually moving.
            for (var i = 0; i < resultCount; ++i)
            {
                var contact = _contacts[i];
                var distance = contact.distance;

                // Are we actually moving?
                if (distance > Mathf.Epsilon)
                {
                    Body.MovePosition(Body.position + (direction * distance));
                    return true;
                }
                // If we're moving into a contact then finish as we cannot move.
                else if (Vector2.Dot(contact.normal, direction) < 0)
                {
                    return false;
                }

            }

            Body.MovePosition(Body.position + velocity);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Call it in fixedUpdate in order to move rigidbody in direction to desired position
    /// </summary>
    public override void Move(Vector3 position)
    {
        if (Body != null)
        {
            var input = transform.position.CalculateDirection(position);
            Move(input.x, input.y);
        }
    }

    void DebugLog(string msg)
    {
        if (_enableDebug)
            Debug.Log(msg);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Top Down Character Controller for non-kinematic rigidbodies
/// </summary>
public class TopDownCharacterControllerNonKinematic : TopDownController
{
    /// <summary>
    /// Call it in fixedUpdate in order to move rigidbody according to axies input
    /// </summary>
    /// <param name="horizontal"></param>
    /// <param name="vertical"></param>
    public override void Move(float horizontal, float vertical)
    {
        if (Body != null)
        {
            Vector2 velocity = new Vector2(horizontal, vertical).normalized * Speed;
            Body.velocity = velocity;         
        }
    }

    /// <summary>
    /// Call it in fixedUpdate in order to move rigidbody in direction to desired position
    /// </summary>
    public override void Move(Vector3 position)
    {
        if (Body != null)
        {
            var input = transform.position.CalculateDirection(position);
            Move(input.x, input.y);
        }
    }
}

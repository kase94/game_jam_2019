﻿using GameArch.Systems.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownPlayerMovement : MonoBehaviour
{

    [SerializeField] public TopDownController _controller;
    [SerializeField] ControlsConfig _controlsConfig;
    Controls _controls;

    public Controls Controls { get => _controls; set => _controls = value; }
    private bool _isInputActive = true;
    public Vector2 lastInput;
    public Vector2 reallastInput;
    public void SetInputActive(bool isActive)
    {
        _isInputActive = isActive;
    }

    public void Stop()
    {
        _controller.Stop();
    }

    private void Awake()
    {
        lastInput = new Vector2(1, 0);
        if (_controlsConfig != null)
            Controls = _controlsConfig.Register();
    }

    void FixedUpdate()
    {
        if (Controls != null && _isInputActive == true)
        {
            _controller.Move(Controls.GetAxisRaw("Horizontal"), Controls.GetAxisRaw("Vertical"));
            
            var horizontal = Controls.GetAxisRaw("Horizontal");
            var vertical = Controls.GetAxisRaw("Vertical");

            reallastInput = new Vector2(horizontal, vertical).normalized;

            if (horizontal != 0 || vertical != 0)
            {
                
                var h = Mathf.Abs(horizontal) > Mathf.Abs(vertical) ? horizontal : 0;
                var w = Mathf.Abs(vertical) > Mathf.Abs(horizontal) ? vertical : 0;
                if (h == w)
                {
                    h = horizontal;
                    w = 0;
                }
                
                lastInput = new Vector2(h, w).normalized;

            }
        }
    }
}


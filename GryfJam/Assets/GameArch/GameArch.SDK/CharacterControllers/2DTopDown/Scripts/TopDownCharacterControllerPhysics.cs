﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Top Down Character Controller for non-kinematic rigidbodies
/// </summary>
public class TopDownCharacterControllerPhysics : TopDownController
{
    [SerializeField] private float _maxVelocity = 10f;
    [SerializeField] private float _brakingPower = 0.80f;

    /// <summary>
    /// Call it in fixedUpdate in order to move rigidbody according to axies input
    /// </summary>
    /// <param name="horizontal"></param>
    /// <param name="vertical"></param>
    public override void Move(float horizontal, float vertical)
    {
        if (Body != null)
        {
            //   Vector2 velocity = new Vector2(horizontal, vertical).normalized * Speed;
            var h = Mathf.Abs(horizontal) > Mathf.Abs(vertical) ? horizontal : 0;
            var w = Mathf.Abs(vertical) > Mathf.Abs(horizontal) ? vertical : 0;
            if (h == w)
            {
                h = horizontal;
                w = 0;
            }
            Vector2 velocity = new Vector2(h, w).normalized * Speed;
            Body.velocity = velocity;
            Body.velocity *= SlowFactor;
    
        }
    }

    /// <summary>
    /// Call it in fixedUpdate in order to move rigidbody in direction to desired position
    /// </summary>
    public override void Move(Vector3 position)
    {
        if (Body != null)
        {
            var input = transform.position.CalculateDirection(position);
            Move(input.x, input.y);
        }
    }
    public override void Stop()
    {
        Body.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    private void Update()
    {
        // decelerate if max velocity reached
        if (Body.velocity.sqrMagnitude > _maxVelocity)
        {
            Body.velocity *= _brakingPower;
        }
    }
}

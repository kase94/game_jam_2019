﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodStainTest : MonoBehaviour {

    public GameObject[] stainsPrefabs;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            int rand = Random.Range(0, stainsPrefabs.Length);
            var GO = Instantiate(stainsPrefabs[rand]);
            GO.transform.position = pos;
        }
    }
}

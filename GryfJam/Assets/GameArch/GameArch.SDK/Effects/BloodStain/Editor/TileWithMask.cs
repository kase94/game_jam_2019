﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "TileWithMask", menuName = "CustomTiles/TileWithMask", order = 1)]
public class TileWithMask : Tile
{
    public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    {

        var mask = go.AddComponent<SpriteMask>();
        mask.sprite = sprite;
        return base.StartUp(position, tilemap, go);
        
    }
}

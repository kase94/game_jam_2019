﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class EditorSceneLoader : MonoBehaviour {
#if UNITY_EDITOR
    private string sceneName;
    private string scenePath;
    [SerializeField]
    private List<SceneReference> scenes;
    private bool isLoaded = false;

    private void Update()
    {
        if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
        {
            foreach (var scene in scenes)
            {
                if (isLoaded == false)
                {
                    if (scene != null)
                    {
                        sceneName = scene.SceneName;
                        scenePath = scene.ScenePath;
                    }
                    if (LoadScene(sceneName, scenePath))
                    {
                        Debug.Log("SceneLoader: " + sceneName + " success.");
                    }
                    else
                    {
                        Debug.Log("SceneLoader: " + sceneName + " already loaded");
                        isLoaded = true;
                    }
                }
            }
        }
    }


    public bool IsSceneCurrentlyLoaded(string sceneName)
    {
        for (int i = 0; i < UnityEditor.SceneManagement.EditorSceneManager.sceneCount; ++i)
        {
            Scene scene = UnityEditor.SceneManagement.EditorSceneManager.GetSceneAt(i);
            if (scene.name == sceneName)
            {
                return true;
            }
        }
        return false;
    }

    public bool LoadScene(string sceneName, string scenePath = "")
    {
        if (IsSceneCurrentlyLoaded(sceneName) == false)
        {
            if (Application.isEditor)
            {
                UnityEditor.SceneManagement.EditorSceneManager.OpenScene(scenePath, UnityEditor.SceneManagement.OpenSceneMode.Additive);
            }
            return true;
        }
        return false;
    }
#endif
}

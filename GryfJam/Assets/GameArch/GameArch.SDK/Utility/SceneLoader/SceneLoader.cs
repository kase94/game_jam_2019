﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    private List<SceneReference> _scenes;

    void Awake()
    {
        foreach (var scene in _scenes)
        {
            if (LoadScene(scene.SceneName))
            {
                Debug.Log("SceneLoader: " + scene + " success.");
            }
            else
            {
                Debug.Log("SceneLoader: " + scene + " already loaded");
            }
        }
    }

    public static bool LoadScene(string sceneName)
    {
        if (IsSceneCurrentlyLoaded(sceneName) == false)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);            
            return true;
        }
        return false;
    }

    public static bool IsSceneCurrentlyLoaded(string sceneName)
    {
        for(int i = 0; i < SceneManager.sceneCount; ++i)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if(scene.name == sceneName)
            {
                return true;
            }
        }

        return false;
    }

}

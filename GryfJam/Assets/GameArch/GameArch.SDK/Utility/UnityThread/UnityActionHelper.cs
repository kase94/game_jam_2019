﻿using System;
using UnityEngine.Events;

public class UnityAcitionHelper
{
    [Serializable]
    public class IntHandler : UnityEvent<int> { }

    [Serializable]
    public class IntIntHandler : UnityEvent<int, int> { }
}

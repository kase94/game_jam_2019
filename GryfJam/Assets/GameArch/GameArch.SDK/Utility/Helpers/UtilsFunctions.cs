﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UtilsFunctions
{
    public static GameObject FindParentWithTag(GameObject childObject, string tag)
    {
        Transform t = childObject.transform;
        while (t.parent != null)
        {
            if (t.parent.tag == tag)
            {
                return t.parent.gameObject;
            }
            t = t.parent.transform;
        }
        return null;
    }

    public static T FindComponentInParent<T>( GameObject childObject)
    {
        Transform t = childObject.transform;
        T component;
        while (t.parent != null)
        {
            component = t.parent.GetComponent<T>();
            if (component != null)
            {
                return component;
            }
            t = t.parent.transform;
        }
        return default(T);
    }

    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        return Quaternion.Euler(angles) * (point - pivot) + pivot;
    }
}

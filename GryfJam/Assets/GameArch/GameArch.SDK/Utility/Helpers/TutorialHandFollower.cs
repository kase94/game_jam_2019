﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHandFollower : PathFollower {

    public float fadeOutSpeed = 0.05f;
    public float fadeInSpeed = 0.05f;
    private SpriteRenderer rend;
    private Color originalColor;

    public enum FadeState
    {
        FadeIn,
        FadeOut,
        StandBy
    }
    FadeState fadeState;
    // Update is called once per frame
    new void FixedUpdate ()
    {
        base.FixedUpdate();

        if (currentPoint == 0)
        {
            fadeState = FadeState.FadeIn;
            transform.position = path[currentPoint].position;
        }
        if (currentPoint == path.Length - 1)
        {
            fadeState = FadeState.FadeOut;
        }

        if (fadeState == FadeState.FadeIn && Time.timeScale > 0)
        {
            rend.color = new Color(originalColor.r, originalColor.g, originalColor.b, Mathf.Lerp(rend.color.a, 1, fadeInSpeed));

            if (rend.color.a == 1)
            {
                fadeState = FadeState.StandBy;
            }
            
        }
       
        if (fadeState == FadeState.FadeOut && Time.timeScale > 0)
        {
            rend.color = new Color(originalColor.r, originalColor.g, originalColor.b,  Mathf.Lerp(rend.color.a, 0, fadeOutSpeed));

            if (rend.color.a == 0)
            {
                fadeState = FadeState.StandBy;
            }
        }

    }

    private new void Awake()
    {
        base.Awake();
        rend = GetComponent<SpriteRenderer>();
        rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, 0);
        originalColor = rend.color;
        fadeState = FadeState.FadeIn;
    }
}

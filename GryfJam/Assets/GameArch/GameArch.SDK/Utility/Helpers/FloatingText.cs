﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {
    [SerializeField]
    private Animator animator;
    private Text floatingText;
	// Use this for initialization
	void OnEnable ()
    {
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        Destroy(gameObject, clipInfo[0].clip.length);
        floatingText = animator.GetComponent<Text>();
	}

    public void SetText(string text)
    {
        floatingText.text = text;
    }
}

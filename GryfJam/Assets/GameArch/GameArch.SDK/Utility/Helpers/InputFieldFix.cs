﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldFix : MonoBehaviour {

    private InputField[] _inputFields;

    private char[] _brokenChars = new char[]
    {
        'ą','Ą','ć','Ć','ę','Ę'
    };

    private void Awake()
    {
        _inputFields = FindObjectsOfType<InputField>();
    }

    private void Update()
    {
        if (Input.anyKey && Input.inputString.Length > 0)
        {
            char missingChar = _brokenChars.Contains(Input.inputString[0]) ? Input.inputString[0] : '\0';

            if (missingChar != '\0')
            {
                for (int i = 0; i < _inputFields.Length; i++)
                {
                    if (_inputFields[i].isFocused)
                    { 
                        string text = _inputFields[i].text;
                        int oldCaretPos = Mathf.Min(_inputFields[i].selectionAnchorPosition, _inputFields[i].selectionFocusPosition);
                        text = text.Remove(oldCaretPos, Mathf.Abs(_inputFields[i].selectionAnchorPosition - _inputFields[i].selectionFocusPosition));
                        text = text.Insert(oldCaretPos, missingChar.ToString());
                        _inputFields[i].text = text;
                        _inputFields[i].caretPosition = oldCaretPos + 1;
                    }
                }
            }
        }
    }
}

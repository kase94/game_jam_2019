﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR 
using UnityEditor;
#endif


public class PathFollower : MonoBehaviour {
    public Transform[] path;
    public float[] pathWait;
    protected float pathWaitAccum = 0;
    public float speed = 5.0f;
    public float reachDist = 1.0f;
    public int currentPoint = 0;
    public MoveStyle moveStyle = MoveStyle.CONSTANT;
    public bool loop = true;
    public float timeBetweenLoops = 0.0f;
    protected float loopWait = 0.0f;
    public bool randomOrder = false;
    public bool snapToStartPosition = false;
    public bool resetOnAwake = false;
    protected bool isFollowing = true;

    public enum MoveStyle
    {
        CONSTANT,
        LERP
    }

    // Use this for initialization
    protected void Awake ()
    {
        if (path.Length > 0 && resetOnAwake == true)
        {
            currentPoint = 0;
            transform.position = path[currentPoint].position;
            isFollowing = true;
        }
    }

    // Update is called once per frame
    protected void FixedUpdate ()
    {
        if (path.Length > 0)
        {
            if (isFollowing)
            {
                float dist = Vector3.Distance(path[currentPoint].position, transform.position);

                if (moveStyle == MoveStyle.CONSTANT)
                {
                    transform.position = Vector3.MoveTowards(transform.position, path[currentPoint].position, Time.fixedDeltaTime * speed);
                }

                if (moveStyle == MoveStyle.LERP)
                {
                    transform.position = Vector3.Lerp(transform.position, path[currentPoint].position, Time.fixedDeltaTime * speed);
                }

                if (dist <= reachDist)
                {
                    if (pathWait != null && pathWait.Length > currentPoint)
                    {
                        pathWaitAccum += Time.fixedDeltaTime;
                        if (pathWaitAccum >= pathWait[currentPoint])
                        {
                            pathWaitAccum = 0;
                        }
                        else
                        {
                            return;
                        }
                    }
                    if (randomOrder)
                    {
                        currentPoint = Random.Range(0, path.Length);
                    }
                    else
                    {
                        currentPoint++;
                    }
                }
            }
            if ((currentPoint >= path.Length) && (false == randomOrder))
            {    
                if (loop == false)
                {
                    isFollowing = false;
                    currentPoint = 0;
                }
                else
                {
                    loopWait += Time.fixedDeltaTime;
                    if(loopWait >= timeBetweenLoops)
                    {
                        loopWait = 0.0f;
                        currentPoint = 0;
                        isFollowing = true;
                        if(snapToStartPosition == true)
                        {
                            transform.position = path[currentPoint].position;
                            currentPoint++;
                        }
                    }
                    else
                    {
                        isFollowing = false;
                    }
                }
            }
        }
	}
#if UNITY_EDITOR
    protected void OnDrawGizmos()
    {
        if(path.Length > 0)
        {
            for(int i = 0; i < path.Length; ++i)
            {
                if(path[i] != null)
                {
                    Gizmos.DrawSphere(path[i].position, reachDist);
                    var defColor = GUI.color;
                    GUI.color = new Color(0,0,0);
                    Vector3 pos = path[i].position;
                    pos.y += reachDist*2;
                    Handles.Label(pos, transform.name + ":" + i.ToString());
                    GUI.color = defColor; 
                }
            }
        }
    }
#endif
}

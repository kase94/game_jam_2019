﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteToMesh : MonoBehaviour {
    public Mesh mesh;

    Mesh ConvertSpriteToMesh(Sprite sprite)
    {
        Mesh mesh = new Mesh();
        mesh.vertices = Array.ConvertAll(sprite.vertices, i => (Vector3)i);
        mesh.uv = sprite.uv;
        mesh.triangles = Array.ConvertAll(sprite.triangles, i => (int)i);

        return mesh;
    }

    private void OnEnable()
    {
        mesh = ConvertSpriteToMesh(gameObject.GetComponent<SpriteRenderer>().sprite);
        ParticleSystem ps = gameObject.GetComponentInChildren<ParticleSystem>();
        var sh = ps.shape;
        sh.enabled = true;
        sh.shapeType = ParticleSystemShapeType.Mesh;
        sh.mesh = mesh;
       
    }
}

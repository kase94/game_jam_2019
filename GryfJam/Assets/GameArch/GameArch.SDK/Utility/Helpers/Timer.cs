﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameArch.Helpers
{
    [Serializable]
    public class Timer
    {
        #region Fields
        public float Duration
        {
            get
            {
                return _duration;
            }
            set
            {
                _duration = value;
            }
        }

        public float TimeStamp
        {
            get
            {
                return _timer;
            }

            protected set
            {
                _timer = value;
            }
        }

        public bool accumulateTimeBetweenRuns = false;

        [SerializeField]
        private float _duration;
        private float _timer = 0;
        #endregion

        #region Methods

        public Timer(float duration, bool accumulate = false)
        {
            Duration = duration;
            accumulateTimeBetweenRuns = accumulate;
        }

        public bool Run(float deltaTime)
        {
            _timer += deltaTime;
            if (_timer >= Duration)
            {
                if (accumulateTimeBetweenRuns)
                {
                    _timer -= Duration;
                }
                else
                {
                    _timer = 0;
                }
                return true;
            }
            return false;
        }

        public void Reset()
        {
            TimeStamp = 0;
        }

        #endregion
    }
}

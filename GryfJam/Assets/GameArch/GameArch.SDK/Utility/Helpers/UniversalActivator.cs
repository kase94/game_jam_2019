﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalActivator : MonoBehaviour {
    public float activationTime = 2.0f;
    public GameObject objectToActivate;
	// Use this for initialization
	void Start ()
    {
        Invoke("ActivateObject", activationTime);
	}

	void ActivateObject ()
    {
        if(objectToActivate != null)
        {
            objectToActivate.SetActive(true);
        }
        Debug.Log("activation");
    }
}

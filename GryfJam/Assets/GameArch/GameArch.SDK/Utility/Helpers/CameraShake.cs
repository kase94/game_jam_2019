﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : Singleton<CameraShake> {

    public float magnitude = 2f;
    public float frequency = 10f;
    public float duration = 2f;
    public float singleShakeSpeed = 1;
    [SerializeField]
    private GameObject shakeObject;
    [SerializeField]
    private Canvas canvas;
    private float timeAcc = 0;
   [SerializeField]
    private bool isShaking = false;
    private Vector3 origin;
    private Vector3 newPos;

    protected void Start()
    {
        origin = shakeObject.transform.position;
        newPos = origin;
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
		if(isShaking)
        {
            timeAcc += Time.fixedDeltaTime;

            if(Vector3.Distance(shakeObject.transform.position, newPos) < 0.1f)
            {
                newPos = origin + PerlinShake();
                shakeObject.transform.position = Vector3.MoveTowards(newPos, shakeObject.transform.position, singleShakeSpeed * Time.fixedDeltaTime);
                if(canvas)
                canvas.renderMode = RenderMode.WorldSpace;
            }

            if (timeAcc >= duration)
            {
                isShaking = false;
                timeAcc = 0f;
                shakeObject.transform.position = origin;
                newPos = origin;
                if (canvas)
                    canvas.renderMode = RenderMode.ScreenSpaceCamera;
            }   
        }
	}

    public void Shake()
    {
        isShaking = true;
        timeAcc = 0f;
    }
    private Vector3 PerlinShake()
    {
        Vector3 result;
        float seed = Time.time * frequency;
        result.x = Mathf.Clamp01(Mathf.PerlinNoise(seed, 0f)) - 0.5f;
        result.y = Mathf.Clamp01(Mathf.PerlinNoise(seed, 0f)) - 0.5f;
        result.z = -10;
        result *= magnitude;
        return result;
    }
}

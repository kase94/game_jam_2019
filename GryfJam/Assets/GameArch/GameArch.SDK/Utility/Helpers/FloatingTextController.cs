﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextController : MonoBehaviour {
    [SerializeField]
    private FloatingText floatingTextPrefab;
    private static FloatingText sFloatingTextPrefab;
    private static Canvas canvas;

    private void Awake()
    {
        canvas = gameObject.GetComponent<Canvas>();
        sFloatingTextPrefab = floatingTextPrefab;
    }

    private void Start()
    {
       
    }

    public static void CreateFloatingText(string text, Vector3 position)
    {
        FloatingText floatingText = Instantiate(sFloatingTextPrefab);
        floatingText.transform.SetParent(canvas.transform, false);
        floatingText.SetText(text);
        floatingText.transform.position = position + new Vector3(Random.Range(-0.5f,0.5f), Random.Range(-0.5f, 0.5f),0);

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ActionQueue : MonoBehaviour
{
    protected Queue<Action> _taskQue = new Queue<Action>();
    protected bool _isRunning = false;

    public void AddAction(Action action, bool start = true)
    {
        _taskQue.Enqueue(() => {
            _isRunning = true;
            action();
            OnActionFinish();
        });

        if (start)
        {
            if (_isRunning == false)
            {
                OnActionFinish();
            }
        }
    }

    public void AddAction(IEnumerator coroutine, bool start = true)
    {
        _taskQue.Enqueue(() => {
            UnityThread.ExecuteCoroutine( CoroutineTemplate(coroutine));
        });

        if (start)
        {
            if (_isRunning == false)
            {
                OnActionFinish();
            }
        }
    }

    private IEnumerator CoroutineTemplate(IEnumerator coroutine)
    {
        _isRunning = true;
        yield return StartCoroutine(coroutine);
        OnActionFinish();
    }

    private void OnActionFinish()
    {
        if(_taskQue.Count > 0)
        {
            ThreadPool.QueueUserWorkItem(  o => _taskQue.Dequeue()() );
        }
        else
        {
            _isRunning = false;
        }
    }
}

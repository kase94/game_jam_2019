﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeRotation : MonoBehaviour {

    private Vector3 _rotation;
    public bool x, y, z = false;
    
    private void Awake()
    {
        _rotation = transform.eulerAngles;
    }

    private void LateUpdate()
    {
        _rotation.x = x ? _rotation.x : transform.eulerAngles.x;
        _rotation.y = y ? _rotation.y : transform.eulerAngles.y;
        _rotation.z = z ? _rotation.z : transform.eulerAngles.z;
        transform.eulerAngles = _rotation;
    }
}

﻿using System;
using System.Collections.Generic;

public static class MathHelper
{
    public static bool IsEqual(float a, float b, float eps = 0.00001f)
    {
        if (Math.Abs(a - b) < eps)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static int[] GetUniqueRandomArray(int min, int max, int count)
    {
        int[] result = new int[count];
        List<int> numbersInOrder = new List<int>();
        for (var x = min; x < max; x++)
        {
            numbersInOrder.Add(x);
        }
        for (var x = 0; x < count; x++)
        {
            var randomIndex = UnityEngine.Random.Range(0, numbersInOrder.Count);
            result[x] = numbersInOrder[randomIndex];
            numbersInOrder.RemoveAt(randomIndex);
        }

        return result;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObject : MonoBehaviour {
    protected GameObject parentObject;
    protected int poolKey;
    protected Dictionary<int, Queue<GameObject>> poolDictionary;
    protected bool reAttach = false;

    public void Init(GameObject parent, int key, Dictionary<int, Queue<GameObject>> dictionary)
    {
        parentObject = parent;
        poolKey = key;
        poolDictionary = dictionary;
        transform.SetParent(parentObject.transform);
    }

    private void OnDisable()
    { 
        Invoke("ReAttach", 0);
    }

    public void ReAttach()
    {
        if (reAttach == true && transform.parent != parentObject.transform && !poolDictionary[poolKey].Contains(gameObject))
        {
            transform.SetParent(parentObject.transform);
            poolDictionary[poolKey].Enqueue(gameObject);
        }
        reAttach = true;
    }

    private void OnDestroy()
    {
        reAttach = false;
    }
}

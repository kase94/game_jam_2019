﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    [SerializeField]
    protected List<ObjectPool> objectPool;
    Dictionary<int, Queue<GameObject>> poolDictionary;
    static ObjectPooler _Instance;

    [Serializable]
    protected class ObjectPool
    {
        public GameObject prefab;
        public int size;
    }

    public static ObjectPooler instance
    {
        get
        {
            if(_Instance == null)
            {
                _Instance = FindObjectOfType<ObjectPooler>();
            }
            return _Instance;
        }
    }

    private void Awake()
    {
        poolDictionary = new Dictionary<int, Queue<GameObject>>();
    }

    // Use this for initialization
    void Start ()
    {
		for(int i = 0; i < objectPool.Count; ++i)
        {
            CreatePool(objectPool[i].prefab, objectPool[i].size);
        }
	}
	
    public void CreatePool(GameObject prefab, int poolSize)
    {
        int poolKey = prefab.GetInstanceID();
        //Debug.Log("CreatePool: " + prefab.name + " id: " + poolKey);
        if (!poolDictionary.ContainsKey(poolKey))
        {
            poolDictionary.Add(poolKey, new Queue<GameObject>());

            GameObject poolHolder = new GameObject(prefab.name + " pool");
            poolHolder.transform.parent = transform;

            for (int i = 0; i < poolSize; ++i)
            {
                GameObject newObject = (Instantiate(prefab) as GameObject);
                PoolObject poolObject = newObject.AddComponent<PoolObject>();
                poolObject.Init(poolHolder, poolKey, poolDictionary);
                newObject.SetActive(false);
                poolDictionary[poolKey].Enqueue(newObject);    
            }
        }
    }

    public GameObject ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        int poolKey = prefab.GetInstanceID();
        GameObject objectToReuse = null;

        if (poolDictionary.ContainsKey(poolKey) && poolDictionary[poolKey].Count > 0 && poolDictionary[poolKey].Peek().activeInHierarchy == false)
        {
            objectToReuse = poolDictionary[poolKey].Dequeue();
            Reuse(objectToReuse, position, rotation);
            //Debug.Log("Successfull reuseObject: " + prefab.name + " " + prefab.GetInstanceID());
        }
        else
        {
            Debug.LogWarning("Cannot reuseObject: " + prefab.name + " "+ prefab.GetInstanceID()  + " pool limit reached");
        }

        return objectToReuse;
    }

    public void Reuse(GameObject reuseObject, Vector3 position, Quaternion rotation)
    {
        reuseObject.SetActive(true);
        reuseObject.transform.position = position;
        reuseObject.transform.rotation = rotation;
    }

    private void OnDestroy()
    {
        _Instance = null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions {
    /// <summary>
    /// Changes layer of gameobject and all it's children
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="layer"></param>
	public static void ChangeLayer(this GameObject gameObject, int layer)
    {
        gameObject.layer = layer;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            gameObject.transform.GetChild(i).gameObject.layer = layer;
        }
    }
}

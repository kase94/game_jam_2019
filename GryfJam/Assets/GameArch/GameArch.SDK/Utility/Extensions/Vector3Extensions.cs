﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions
{
    public static Vector3 CalculateDirection(this Vector3 a, Vector3 b)
    {
        return (b - a).normalized;
    }

    public static Vector3 CalculateMouseDirection(this Vector3 position, Vector3 mousePosition)
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(mousePosition);
        mousePos.z = position.z;
        return position.CalculateDirection(mousePos);
    }
}

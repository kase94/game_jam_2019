﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEngine;

public static class StringExtensions {
    private static Dictionary<char, char> _diacritics = new Dictionary<char, char>()
    {
        {'ą','a'}, {'Ą','A'},
        {'ć','c'}, {'Ć','C'},
        {'ę','e'}, {'Ę','E'},
        {'ś','s'}, {'Ś','S'},
        {'ó','o'}, {'Ó','O'},
        {'ż','z'}, {'Ż','Z'},
        {'ź','z'}, {'Ź','Z'},
        {'ń','n'}, {'Ń','N'},      
        {'ł','l'}, {'Ł','L'}

    };

    public static string RemoveDiacritics(this string str)
    {
        if (str == null) return null;
        foreach (var d in _diacritics)
        {
            str = str.Replace(d.Key, d.Value);
        }
        return str;
    }
}

﻿using UnityEngine;
using UnityEditor;

//namespace GameArch.Systems.Input
//{
//    [CustomEditor(typeof(KeyMapping))]
//    public class KeyMappingEditor : Editor
//    {
//        KeyMapping comp;
//
//        public void OnEnable()
//        {
//            comp = (KeyMapping)target;
//        }
//
//        public override void OnInspectorGUI()
//        {
//
//            Draw(ref comp.PrimaryInput);
//            Draw(ref comp.SecondaryInput);
//            Draw(ref comp.ThirdInput);
//        }
//
//        public void Draw(ref CustomInputSO customInput)
//        {
//            var editor = Editor.CreateEditor(customInput);
//            if (editor != null)
//            {
//                editor.DrawDefaultInspectorWithoutScriptField();
//            }
//            if (GUILayout.Button("Add Keyboard"))
//                customInput = CreateInstance<KeyboardInputSO>();
//            if (GUILayout.Button("Add Joystick"))
//                customInput = CreateInstance<JoystickInputSO>();
//            if (GUILayout.Button("Add Mouse"))
//                customInput = CreateInstance<MouseInputSO>();
//            if (GUILayout.Button("Remove"))
//                customInput = CreateInstance<CustomInputSO>();
//        }
//    }
//}
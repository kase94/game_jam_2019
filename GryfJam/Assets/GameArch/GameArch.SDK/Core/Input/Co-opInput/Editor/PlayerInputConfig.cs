﻿using GameArch.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems
{
    [CreateAssetMenu(fileName = "PlayerInputConfig", menuName = "GameArch/Input/Co-op Player Input Config", order = 1)]
    public class PlayerInputConfig : ScriptableObject
    {
        [SerializeField]
        private StringStringDictionary stringStringStore = StringStringDictionary.New<StringStringDictionary>();
        private Dictionary<string, string> stringStrings
        {
            get { return stringStringStore.dictionary; }
        }
    }
}
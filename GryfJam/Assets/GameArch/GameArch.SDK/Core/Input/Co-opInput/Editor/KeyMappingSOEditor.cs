﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;

namespace GameArch.Systems.Input
{
    [CustomEditor(typeof(KeyMappingSO))]
    public class KeyMappingSOEditor : Editor
    {
        KeyMappingSO comp;

        InputType _primaryInputType;
        InputType _secondaryInputType;
        InputType _thirdInputType;

        [Serializable]
        public enum InputType
        {
            Keyboard,
            Joystick,
            Mouse
        } 

        public void OnEnable()
        {
            Init();
        }

        private void Init()
        {
            if(comp == null)
            {
                comp = target as KeyMappingSO;
                if (comp != null)
                {
                    _primaryInputType = CustomInputToEnum(ref comp.PrimaryInput);
                    _secondaryInputType = CustomInputToEnum(ref comp.SecondaryInput);
                    _thirdInputType = CustomInputToEnum(ref comp.ThirdInput);
                }
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            Init();
            if (EditorUtility.IsPersistent(comp))
            {
                comp.Parent = comp;
            }

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Name", GUILayout.Width(70f));
            SerializedProperty prop = serializedObject.FindProperty("Name");
            EditorGUILayout.PropertyField(prop, GUIContent.none, GUILayout.Width(200f));

            EditorGUILayout.EndHorizontal();

            Draw("Primary", ref comp.PrimaryInput, ref _primaryInputType);
            Draw("Secondary",ref comp.SecondaryInput, ref _secondaryInputType);
            Draw("Third",ref comp.ThirdInput, ref _thirdInputType);
      
            serializedObject.ApplyModifiedProperties();
        }

        public void Draw(string name, ref CustomInputSO customInput, ref InputType inputType)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(name, GUILayout.Width(70f));
            inputType = (InputType)EditorGUILayout.EnumPopup( inputType, GUILayout.Width(80f));

            if (EditorUtility.IsPersistent(comp.Parent) && TypeofEnumInputType(inputType) != customInput.GetType())
            {
                if (customInput != null)
                    AssetDatabase.RemoveObjectFromAsset(customInput);
                
                switch (inputType)
                {
                    case InputType.Keyboard:
                        customInput = CreateInstance<KeyboardInputSO>();
                        AssetDatabase.AddObjectToAsset(customInput, comp.Parent);
                        AssetDatabase.SaveAssets();
                        break;
                    case InputType.Joystick:
                        customInput = CreateInstance<JoystickInputSO>();
                        AssetDatabase.AddObjectToAsset(customInput, comp.Parent);
                        AssetDatabase.SaveAssets();
                        break;
                    case InputType.Mouse:
                        customInput = CreateInstance<MouseInputSO>();
                        AssetDatabase.AddObjectToAsset(customInput, comp.Parent);
                        AssetDatabase.SaveAssets();
                        break;
                    default:
                        break;
                }
            }

            var editor = Editor.CreateEditor(customInput);
            if (editor != null)
            {
                editor.DrawDefaultInspectorWithoutScriptField();
            }

            if (GUI.changed) EditorUtility.SetDirty(comp);

            EditorGUILayout.EndHorizontal();

        }

        private Type TypeofEnumInputType(InputType inputType)
        {
            switch (inputType)
            {
                case InputType.Keyboard:
                    return typeof(KeyboardInputSO);
                case InputType.Joystick:
                    return typeof(JoystickInputSO);
                case InputType.Mouse:
                    return typeof(MouseInputSO);
                default:
                    return null;
            }
        }

        private InputType CustomInputToEnum(ref CustomInputSO customInput)
        {
            if(customInput == null)
                return InputType.Keyboard;

            Type type = customInput.GetType();

            if (type == typeof(MouseInputSO)){
                return InputType.Mouse;
            } else if (type == typeof(JoystickInputSO)){
                return InputType.Joystick;
            }
            else 
                return InputType.Keyboard;
        }
    }
}
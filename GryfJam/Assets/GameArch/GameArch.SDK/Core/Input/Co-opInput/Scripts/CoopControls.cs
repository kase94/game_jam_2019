﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace GameArch.Systems.Input
{
    public class CoopControls : Singleton<CoopControls>
    {
        [SerializeField]
        List<ControlsConfig> _playersControlsConfig;
        [SerializeField]
        List<ControlsConfig> _joystickPlayersControlsConfig;

        List<Controls> _playersControls = new List<Controls>();

        public ReadOnlyCollection<Controls> GetPlayersControls()
        {
            return _playersControls.AsReadOnly();
        }

        private void InitCoopControls()
        {
            for (int i = 0; i < _playersControlsConfig.Count; i++)
            {
                var controls = InputControl.SetControls($"player_{i+1}");

                foreach (var item in _playersControlsConfig[i].KeyMappingSOList)
                {
                    controls.SetKey(item.Name, item.PrimaryInput.GetInput(), item.SecondaryInput.GetInput(), item.ThirdInput.GetInput() );
                }

                foreach (var axis in _playersControlsConfig[i].Axes)
                {
                    controls.SetAxis(axis.Name, axis.Negative, axis.Positive);
                }

                _playersControls.Add(controls);
            }

            for (int i = 0; i < InputControl.GetJoystickCount(); i++)
            {
                for (int j = 0; j < _joystickPlayersControlsConfig.Count; j++)
                {
                    var controls = InputControl.SetControls($"player_{_playersControls.Count + 1}");

                    foreach (var item in _joystickPlayersControlsConfig[j].KeyMappingSOList)
                    {
                        CustomInput prim = item.PrimaryInput.GetInput();
                        CustomInput sec = item.SecondaryInput.GetInput();
                        CustomInput third = item.ThirdInput.GetInput();

                        SetJoystickTarget(ref prim, Joystick.Joystick1 + i);
                        SetJoystickTarget(ref sec, Joystick.Joystick1 + i);
                        SetJoystickTarget(ref third, Joystick.Joystick1 + i);
                       
                        Debug.LogWarning($"Button: {item.Name}" + prim.ToString());
                        controls.SetKey(item.Name, prim, sec, third);
                    }

                    foreach (var axis in _joystickPlayersControlsConfig[j].Axes)
                    {
                        controls.SetAxis(axis.Name, axis.Negative, axis.Positive);
                    }

                    _playersControls.Add(controls);
                }

            }


            Debug.Log($"Players controls count: {_playersControls.Count}");
            Debug.Log($"Joysticks count: {InputControl.GetJoystickCount()}");
            Debug.Log($"Joysticks names count: {InputControl.GetJoystickNames().Length}");
        }

        private void SetJoystickTarget(ref CustomInput customInput, Joystick target)
        {
            if (customInput.GetType() == typeof(JoystickInput))
            {
                JoystickInput primaryjoy = (JoystickInput)customInput;
                if (primaryjoy.type == JoystickInput.InputType.Button)
                {
                    customInput = new JoystickInput(primaryjoy.button, target);
                }
                else
                {
                    customInput = new JoystickInput(primaryjoy.axis, target);
                }
          
            }
        }

        void Awake()
        {
            InitCoopControls();
        }

        // Update is called once per frame
        void Update()
        {
            
            foreach (var controls in _playersControls)
            {
                foreach (var key in controls.GetKeys())
                {
               //     Debug.Log($"Player {controls.Name}  {key.name}");
                    if (controls.GetButtonDown(key))
                    {
                        Debug.Log($"Player {controls.Name} pressed {key.name}");
                    }
                }
            }
            
            
        }
    }
}

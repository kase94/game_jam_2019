﻿using GameArch.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [CreateAssetMenu(fileName = "Co-opPlayerLobbyConfig", menuName = "GameArch/Input/Co-op Player Lobby Config", order = 1)]
    public class CoopPlayerLobbyConfig : ScriptableObject
    {
        #region Fields
        [SerializeField]
        private string _joinButton, _startGameButton;
        #endregion

        #region Properties
        public string JoinButton { get => _joinButton; set => _joinButton = value; }
        public string StartGameButton { get => _startGameButton; set => _startGameButton = value; }
        #endregion
    }
}
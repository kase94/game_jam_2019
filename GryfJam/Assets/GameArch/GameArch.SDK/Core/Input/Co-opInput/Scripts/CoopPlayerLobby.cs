﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.Events;

namespace GameArch.Systems.Input
{
    public class CoopPlayerLobby : Singleton<CoopPlayerLobby>
    {
        #region Inspector Fields
        [SerializeField] CoopPlayerLobbyConfig _config;
        [SerializeField] List<PlayerJoinWindow> _playerJoinWindows;
        [SerializeField] GameObject _startGameTooltip;
        #endregion

        #region Private fields
        private List<PlayerJoinWindow> _unregisteredPlayerJoinWindows;
        private ReadOnlyCollection<Controls> _allPlayers;
        private List<Controls> _unregisteredPlayers;
        private List<string> _registeredPlayers = new List<string>();
        private Dictionary<string, int> _registeredPlayersHeads = new Dictionary<string, int>();
        private bool _isStarted;
        #endregion

        #region Public fields
        public CoopPlayerLobbyConfig Config { get => _config; private set => _config = value; }
        public Action OnStart;
        #endregion

        #region Methods
        void Start()
        {
            _unregisteredPlayers = new List<Controls> (CoopControls.Instance.GetPlayersControls());
            _allPlayers = CoopControls.Instance.GetPlayersControls();
            _unregisteredPlayerJoinWindows = new List<PlayerJoinWindow>(_playerJoinWindows);
            foreach (var joinWindow in _playerJoinWindows)
            {
                joinWindow.OnReady += OnPlayerReady;
            }
        }

        private void OnDestroy()
        {
            foreach (var joinWindow in _playerJoinWindows)
            {
                joinWindow.OnReady -= OnPlayerReady;
            }
        }

        private void Awake()
        {
            
        }

        private void OnPlayerReady(string controls)
        {
            Debug.Log("OnPlayerReady");
            _registeredPlayers.Add(controls);
            if (_registeredPlayers.Count == 2)
            {
                _startGameTooltip.SetActive(true);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_unregisteredPlayers != null)
            {
                for (int i = _unregisteredPlayers.Count - 1; i >= 0; --i)
                {
                    if (_unregisteredPlayers[i].GetButton(Config.JoinButton))
                    {
                        if (_unregisteredPlayerJoinWindows.Count > 0)
                        {
                            var control = _unregisteredPlayers[i];
                            _unregisteredPlayerJoinWindows[0].SetControls(ref control);
                            _unregisteredPlayerJoinWindows.RemoveAt(0);
                            _unregisteredPlayers.Remove(_unregisteredPlayers[i]);
                        }
                    }
                }
            }

            if (_allPlayers != null && _isStarted == false && _registeredPlayers.Count > 1)
            {
                foreach (var player in _allPlayers)
                {
                    if (player.GetButton(Config.StartGameButton))
                    {
                        _isStarted = true;
                        OnStart?.Invoke();
                        foreach (var joinWindow in _playerJoinWindows)
                        {
                            if (joinWindow.State == PlayerJoinWindow.JoinState.Ready)
                                _registeredPlayersHeads.Add(joinWindow._controls.Name, joinWindow.GetAvatarID());
                        }
                        EventManager.Instance.TriggerEvent(GetIdentity(), Events.GAME_START.name, Events.GAME_START.CreateData(_registeredPlayersHeads), false);
                        Debug.Log("CoopPlayerLobby: Game Start!");
                        break;
                    }
                }
            }
        }
        #endregion

        #region EventManager

        public static Identity GetIdentity()
        {
            return Identity.By.Name(typeof(CoopPlayerLobby).ToString());
        }

        public static class Events
        {
            public static class GAME_START
            {
                public const string name = "Game Start";

                internal static ActionParams CreateData(Dictionary<string, int> registeredPlayersControls)
                {
                    ActionParams playersParams = new ActionParams();
                    playersParams.Add<Dictionary<string, int>>("players", registeredPlayersControls);
                    return playersParams;
                }

                public static Dictionary<string, int> GetData(ActionParams playersParams)
                {
                    return playersParams.As<Dictionary<string, int>>("players");
                }
            }
        }

        #endregion
    }
}

﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameArch.Systems.Input
{
    public class PlayerJoinWindow : MonoBehaviour
    {
        [SerializeField] Image _fillImage;
        [SerializeField] Image _bgImage;
        [SerializeField] TextMeshProUGUI _textMesh;
        [SerializeField] HeadData _headData;
        [SerializeField] Image _avatar;
        private int _currentAvatar;
        private bool _gameStarted;
        public Controls _controls;
        private JoinState _state;

        public enum JoinState
        {
            Unregistered,
            Registered,
            Joining,
            Ready
        }

        public JoinState State{ get => _state; private set => _state = value; }
        public Action<string> OnReady;

        private void Awake()
        {
            CoopPlayerLobby.Instance.OnStart += OnStart;
        }

        private void OnStart()
        {
            _gameStarted = true;
           
        }

        public void SetControls(ref Controls controls)
        {
            _controls = controls;
            State = JoinState.Registered;
        }

        public int GetAvatarID()
        {
            return _currentAvatar;
        }
        // Update is called once per frame
        void Update()
        {

            if (_gameStarted == false && State == JoinState.Ready)
            {
                if (_controls.GetButtonDown("Left"))
                {
                    _currentAvatar = _currentAvatar > 0 ? --_currentAvatar : _headData._headsData.Count - 1;
                }
                else if (_controls.GetButtonDown("Right"))
                {
                    _currentAvatar = _currentAvatar < _headData._headsData.Count - 1 ? ++_currentAvatar : 0;
                }
                _avatar.sprite = _headData._headsData[_currentAvatar].normal;
            }

            // Join mechanism
            if (_gameStarted == false && (State == JoinState.Registered || State == JoinState.Joining))
            {
                if (_controls.GetButton(CoopPlayerLobby.Instance.Config.JoinButton))
                {
                    _fillImage.fillAmount += 1.2f * Time.deltaTime;

                    if (_fillImage.fillAmount >= 1)
                    {
                        State = JoinState.Ready;
                        _textMesh.text = "Ready!";
                        OnReady?.Invoke(_controls.Name);
                        _avatar.enabled = true;
                    }
                    else
                    {
                        State = JoinState.Joining;
                    }
                }
                else if (_fillImage.fillAmount < 1 && _fillImage.fillAmount > 0)
                {
                    _fillImage.fillAmount -= 1.6f * Time.deltaTime;
                    State = JoinState.Registered;
                }
            }
        }
    }
}

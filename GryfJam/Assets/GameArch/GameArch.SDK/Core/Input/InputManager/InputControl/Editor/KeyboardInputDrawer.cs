﻿using UnityEditor;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [CustomPropertyDrawer(typeof(KeyboardInput), true)]
    public class KeyboardInputDrawer : PropertyDrawer
    {
        private SerializedProperty _mKey;
        private SerializedProperty _mModifiers;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), GUIContent.none);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var keyRect = new Rect(position.x, position.y, 155, position.height);
            var modRect = new Rect(position.x + 160, position.y, 100, position.height);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            EditorGUI.PropertyField(keyRect, property.FindPropertyRelative("mKey"), GUIContent.none);
            EditorGUI.PropertyField(modRect, property.FindPropertyRelative("mModifiers"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}
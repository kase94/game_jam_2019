﻿using UnityEditor;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [CustomPropertyDrawer(typeof(JoystickInput), true)]
    public class JoystickInputDrawer : PropertyDrawer
    {
        private SerializedProperty _mAxis;
        private SerializedProperty _mButton;
        private SerializedProperty _mModifiers;
        private SerializedProperty _mInputType;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), GUIContent.none);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects

            var typeRect = new Rect(position.x, position.y, 50, position.height);
            var inputRect = new Rect(position.x + 55, position.y, 100, position.height);
            var modRect = new Rect(position.x + 160, position.y, 100, position.height);

            _mInputType = property.FindPropertyRelative("mType");
            // Draw fields - passs GUIContent.none to each so they are drawn without labels

            EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("mType"), GUIContent.none);

            if (_mInputType.enumValueIndex == (int)MouseInput.InputType.Axis)
            {
                EditorGUI.PropertyField(inputRect, property.FindPropertyRelative("mAxis"), GUIContent.none);
            }
            else
            {
                EditorGUI.PropertyField(inputRect, property.FindPropertyRelative("mButton"), GUIContent.none);
            }

            EditorGUI.PropertyField(modRect, property.FindPropertyRelative("mModifiers"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [CreateAssetMenu(fileName = "MouseInputSO", menuName = "GameArch/Input/MouseInputSO", order = 2)]
    public class MouseInputSO : CustomInputSO
    {
        [SerializeField]
        private MouseInput MouseInput;

        public override CustomInput GetInput()
        {
            return MouseInput;
        }
    }
}
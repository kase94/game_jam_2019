﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems.Input
{
    public class KeyboardInputSO : CustomInputSO
    {
        [SerializeField]
        private KeyboardInput KeyboardInput;

        public override CustomInput GetInput()
        {
            return KeyboardInput;
        }
    }
}
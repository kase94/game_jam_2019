using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GameArch.Systems.Input
{
    /// <summary>
    /// <see cref="InputControl"/> provide interface to the Input system. It's based on <see cref="Input"/> class and allow to change key mappings at runtime.
    /// </summary>
    public static class InputControl
    {
        // Set of Controls
        private static List<Controls> mControlsList = new List<Controls>();
        private static Dictionary<string, Controls> mControlsDict = new Dictionary<string, Controls>();

        #region Options Fields
        // Joystick options
        private static float sJoystickThreshold = 0.2f;

        // Mouse options
        private static float sMouseSensitivity = 1f;
        private static bool sInvertMouseY = false;
        #endregion

        #region Properties

        #region joystickThreshold
        /// <summary>
        /// Gets or sets the joystick threshold.
        /// </summary>
        /// <value>Joystick threshold.</value>
        public static float joystickThreshold
        {
            get
            {
                return sJoystickThreshold;
            }

            set
            {
                if (value < 0f)
                {
                    sJoystickThreshold = 0f;
                }
                else
                if (value > 1f)
                {
                    sJoystickThreshold = 1f;
                }
                else
                {
                    sJoystickThreshold = value;
                }
            }
        }
        #endregion

        #region mouseSensitivity
        /// <summary>
        /// Gets or sets the mouse sensitivity.
        /// </summary>
        /// <value>Mouse sensitivity.</value>
        public static float mouseSensitivity
        {
            get
            {
                return sMouseSensitivity;
            }

            set
            {
                if (value < 0f)
                {
                    sMouseSensitivity = 0f;
                }
                else
                {
                    sMouseSensitivity = value;
                }
            }
        }
        #endregion

        #region invertMouseY
        /// <summary>
        /// Gets or sets a value indicating that mouse Y is inverted.
        /// </summary>
        /// <value><c>true</c> if mouse Y is inverted; otherwise, <c>false</c>.</value>
        public static bool invertMouseY
        {
            get
            {
                return sInvertMouseY;
            }

            set
            {
                sInvertMouseY = value;
            }
        }
        #endregion

        #endregion

        #region Controls API
        public static Controls SetControls(string name)
        {
            Controls controls = null;

            if (mControlsDict.ContainsKey(name))
            {
                controls = mControlsDict[name];
            }
            else
            {
                controls = null ?? new Controls(name);
                mControlsDict.Add(name, controls);
                mControlsList.Add(controls);
            }
            
            return controls;
        }

        public static Controls SetControls(Controls controls)
        {
            if (mControlsDict.ContainsKey(controls.Name))
            {
                mControlsDict[controls.Name] = controls;
            }
            else
            {
                mControlsDict.Add(controls.Name, controls);
                mControlsList.Add(controls);
            }

            return controls;
        }


        /// <summary>
        /// Removes <see cref="Controls"/> by name.
        /// </summary>
        /// <param name="name">Controls name.</param>
        public static void RemoveControls(string name)
        {
            Controls outControls = null;

            if (mControlsDict.TryGetValue(name, out outControls))
            {
                RemoveControls(outControls);
            }
        }

        /// <summary>
        /// Removes specified <see cref="Controls"/>.
        /// </summary>
        /// <param name="controls">Controls instance.</param>
        public static void RemoveControls(Controls controls)
        {
            bool good = true;

            if (!mControlsList.Remove(controls))
            {
                good = false;
            }

            if (!mControlsDict.Remove(controls.Name))
            {
                good = false;
            }

            if (!good)
            {
                Debug.LogError("Failed to remove axis \"" + controls.Name + "\"");
            }
        }

        /// <summary>
        /// Gets <see cref="Controls"/> by name.
        /// </summary>
        /// <param name="name">Controls name.</param>
        public static Controls GetControlsByName(string name)
        {
            Controls outControls = null;

            if (mControlsDict.TryGetValue(name, out outControls))
            {
                return outControls;
            }

            return null;
        }

        /// <summary>
        /// Gets the list of controls.
        /// </summary>
        /// <returns>List of controls.</returns>
        public static ReadOnlyCollection<Controls> GetControls()
        {
            return mControlsList.AsReadOnly();
        }
        #endregion


        // ----------------------------------------------------------------

        /// <summary>
        /// Gets the last measured linear acceleration of a device in three-dimensional space.
        /// </summary>
        /// <value>Last measured linear acceleration of a device in three-dimensional space.</value>
        public static Vector3 acceleration
        {
            get
            {
                return UnityEngine.Input.acceleration;
            }
        }

        /// <summary>
        /// Gets the number of acceleration measurements which occurred during last frame.
        /// </summary>
        /// <value>Number of acceleration measurements which occurred during last frame.</value>
        public static int accelerationEventCount
        {
            get
            {
                return UnityEngine.Input.accelerationEventCount;
            }
        }

        /// <summary>
        /// Gets the list of acceleration measurements which occurred during the last frame. (Read Only) (Allocates temporary variables).
        /// </summary>
        /// <value>List of acceleration measurements which occurred during the last frame. (Read Only) (Allocates temporary variables).</value>
        public static AccelerationEvent[] accelerationEvents
        {
            get
            {
                return UnityEngine.Input.accelerationEvents;
            }
        }

        /// <summary>
        /// Gets a value indicating that any key or mouse button currently held down.
        /// </summary>
        /// <value><c>true</c> if key or mouse button currently held down; otherwise, <c>false</c>.</value>
        public static bool anyKey
        {
            get
            {
                return UnityEngine.Input.anyKey;
            }
        }

        /// <summary>
        /// Gets a value indicating that if it is a first frame the user hits any key or mouse button.
        /// </summary>
        /// <value><c>true</c> if user press any key during this frame; otherwise, <c>false</c>.</value>
        public static bool anyKeyDown
        {
            get
            {
                return UnityEngine.Input.anyKeyDown;
            }
        }

        /// <summary>
        /// Property for accessing compass (handheld devices only).
        /// </summary>
        /// <value>Handheld device compass.</value>
        public static Compass compass
        {
            get
            {
                return UnityEngine.Input.compass;
            }
        }

        /// <summary>
        /// This property controls if input sensors should be compensated for screen orientation.
        /// </summary>
        /// <value><c>true</c> if input sensors should be compensated for screen orientation; otherwise, <c>false</c>.</value>
        public static bool compensateSensors
        {
            get
            {
                return UnityEngine.Input.compensateSensors;
            }
        }

        /// <summary>
        /// The current text input position used by IMEs to open windows.
        /// </summary>
        /// <value>Text input position.</value>
        public static Vector2 compositionCursorPos
        {
            get
            {
                return UnityEngine.Input.compositionCursorPos;
            }
        }

        /// <summary>
        /// The current IME composition string being typed by the user.
        /// </summary>
        /// <value>Current IME composition string.</value>
        public static string compositionString
        {
            get
            {
                return UnityEngine.Input.compositionString;
            }
        }

        /// <summary>
        /// Returns input that currently active by used.
        /// </summary>
        /// <returns>Currently active input.</returns>
        /// <param name="ignoreMouseMovement">If set to <c>true</c> ignore mouse movement.</param>
        /// <param name="useModifiers">If set to <c>true</c> handle key modifiers.</param>
        public static CustomInput currentInput(bool ignoreMouseMovement = true, bool useModifiers = false)
        {
            KeyModifier modifiers = KeyModifier.NoModifier;

            if (useModifiers)
            {
                if (UnityEngine.Input.GetKey(KeyCode.LeftControl) || UnityEngine.Input.GetKey(KeyCode.RightControl))
                {
                    modifiers |= KeyModifier.Ctrl;
                }

                if (UnityEngine.Input.GetKey(KeyCode.LeftAlt) || UnityEngine.Input.GetKey(KeyCode.RightAlt))
                {
                    modifiers |= KeyModifier.Alt;
                }

                if (UnityEngine.Input.GetKey(KeyCode.LeftShift) || UnityEngine.Input.GetKey(KeyCode.RightShift))
                {
                    modifiers |= KeyModifier.Shift;
                }
            }

            #region Joystick
            for (int i = (int)Joystick.Joystick1; i < (int)Joystick.None; ++i)
            {
                String target = "Joystick " + i.ToString() + " ";

                #region Axes
                for (int j = 1; j <= (int)JoystickAxis.None / 2; ++j)
                {
                    float joyAxis = UnityEngine.Input.GetAxis(target + "Axis " + j.ToString());

                    if (joyAxis < -sJoystickThreshold)
                    {
                        return new JoystickInput((JoystickAxis)((j - 1) * 2 + 1), (Joystick)i, modifiers);
                    }

                    if (joyAxis > sJoystickThreshold)
                    {
                        return new JoystickInput((JoystickAxis)((j - 1) * 2), (Joystick)i, modifiers);
                    }
                }
                #endregion

                #region Buttons
                for (int j = 0; j < (int)JoystickButton.None; ++j)
                {
                    if (UnityEngine.Input.GetButton(target + "Button " + (j + 1).ToString()))
                    {
                        return new JoystickInput((JoystickButton)j, (Joystick)i, modifiers);
                    }
                }
                #endregion
            }
            #endregion

            #region Mouse

            #region Axes

            #region ScrollWheel
            float mouseAxis = UnityEngine.Input.GetAxis("Mouse ScrollWheel");

            if (mouseAxis < 0)
            {
                return new MouseInput(MouseAxis.WheelDown, modifiers);
            }

            if (mouseAxis > 0)
            {
                return new MouseInput(MouseAxis.WheelUp, modifiers);
            }
            #endregion

            if (!ignoreMouseMovement)
            {
                #region X
                mouseAxis = UnityEngine.Input.GetAxis("Mouse X");

                if (mouseAxis < 0)
                {
                    return new MouseInput(MouseAxis.MouseLeft, modifiers);
                }

                if (mouseAxis > 0)
                {
                    return new MouseInput(MouseAxis.MouseRight, modifiers);
                }
                #endregion

                #region Y
                mouseAxis = UnityEngine.Input.GetAxis("Mouse Y");

                if (mouseAxis < 0)
                {
                    return new MouseInput(MouseAxis.MouseDown, modifiers);
                }

                if (mouseAxis > 0)
                {
                    return new MouseInput(MouseAxis.MouseUp, modifiers);
                }
                #endregion
            }
            #endregion

            #region Buttons
            for (int i = 0; i < (int)MouseButton.None; ++i)
            {
                KeyCode key = (KeyCode)((int)KeyCode.Mouse0 + i);

                if (UnityEngine.Input.GetKey(key))
                {
                    return new MouseInput((MouseButton)i, modifiers);
                }
            }
            #endregion

            #endregion

            #region Keyboard
            foreach (var value in Enum.GetValues(typeof(KeyCode)))
            {
                KeyCode key = (KeyCode)value;

                if (
                    (
                     !useModifiers
                     ||
                     (
                      key != KeyCode.LeftControl
                      &&
                      key != KeyCode.RightControl
                      &&
                      key != KeyCode.LeftAlt
                      &&
                      key != KeyCode.RightAlt
                      &&
                      key != KeyCode.LeftShift
                      &&
                      key != KeyCode.RightShift
                      )
                    )
                    &&
                    UnityEngine.Input.GetKey(key)
                   )
                {
                    return new KeyboardInput(key, modifiers);
                }
            }
            #endregion

            return null;
        }

        /// <summary>
        /// Gets the device physical orientation as reported by OS.
        /// </summary>
        /// <value>Device orientation.</value>
        public static DeviceOrientation deviceOrientation
        {
            get
            {
                return UnityEngine.Input.deviceOrientation;
            }
        }

        /// <summary>
        /// Returns specific acceleration measurement which occurred during last frame. (Does not allocate temporary variables).
        /// </summary>
        /// <returns>Specific acceleration measurement which occurred during last frame.</returns>
        /// <param name="index">Index of acceleration event.</param>
        public static AccelerationEvent GetAccelerationEvent(int index)
        {
            return UnityEngine.Input.GetAccelerationEvent(index);
        }

        /// <summary>
        /// Gets the count of connected joysticks.
        /// </summary>
        /// <returns>Count of connected joysticks.</returns>
        public static int GetJoystickCount()
        {
            return UnityEngine.Input.GetJoystickNames().Length;
        }

        /// <summary>
        /// Returns an array of strings describing the connected joysticks.
        /// </summary>
        /// <returns>Names of connected joysticks.</returns>
        public static string[] GetJoystickNames()
        {
            return UnityEngine.Input.GetJoystickNames();
        }

        /// <summary>
        /// Returns true while the user holds down the key identified by name. Think auto fire.
        /// </summary>
        /// <returns><c>true</c>, if key is held down, <c>false</c> otherwise.</returns>
        /// <param name="name">Name of key.</param>
        public static bool GetKey(string name)
        {
            return UnityEngine.Input.GetKey(name);
        }

        /// <summary>
        /// Returns true while the user holds down the key identified by the key <see cref="KeyCode"/> enum parameter.
        /// </summary>
        /// <returns><c>true</c>, if key is held down, <c>false</c> otherwise.</returns>
        /// <param name="key">Code of key.</param>
        public static bool GetKey(KeyCode key)
        {
            return UnityEngine.Input.GetKey(key);
        }

        /// <summary>
        /// Returns true during the frame the user starts pressing down the key identified by name.
        /// </summary>
        /// <returns><c>true</c>, if user starts pressing down the key, <c>false</c> otherwise.</returns>
        /// <param name="name">Name of key.</param>
        public static bool GetKeyDown(string name)
        {
            return UnityEngine.Input.GetKeyDown(name);
        }

        /// <summary>
        /// Returns true during the frame the user starts pressing down the key identified by the key <see cref="KeyCode"/> enum parameter.
        /// </summary>
        /// <returns><c>true</c>, if user starts pressing down the key, <c>false</c> otherwise.</returns>
        /// <param name="name">Code of key.</param>
        public static bool GetKeyDown(KeyCode key)
        {
            return UnityEngine.Input.GetKeyDown(key);
        }

        /// <summary>
        /// Returns true during the frame the user releases the key identified by name.
        /// </summary>
        /// <returns><c>true</c>, if user releases the key, <c>false</c> otherwise.</returns>
        /// <param name="name">Name of key.</param>
        public static bool GetKeyUp(string name)
        {
            return UnityEngine.Input.GetKeyUp(name);
        }

        /// <summary>
        /// Returns true during the frame the user releases the key identified by the key <see cref="KeyCode"/> enum parameter.
        /// </summary>
        /// <returns><c>true</c>, if user releases the key, <c>false</c> otherwise.</returns>
        /// <param name="name">Code of key.</param>
        public static bool GetKeyUp(KeyCode key)
        {
            return UnityEngine.Input.GetKeyUp(key);
        }

        /// <summary>
        /// Returns whether the given mouse button is held down.
        /// </summary>
        /// <returns><c>true</c>, if mouse button is held down, <c>false</c> otherwise.</returns>
        /// <param name="button">Mouse button.</param>
        public static bool GetMouseButton(int button)
        {
            if (button >= 0 && button < (int)MouseButton.None)
            {
                return GetMouseButton((MouseButton)button);
            }

            return false;
        }

        /// <summary>
        /// Returns whether the given <see cref="MouseButton"/> is held down.
        /// </summary>
        /// <returns><c>true</c>, if mouse button is held down, <c>false</c> otherwise.</returns>
        /// <param name="button">Mouse button.</param>
        public static bool GetMouseButton(MouseButton button)
        {
            if (button != MouseButton.None)
            {
                return UnityEngine.Input.GetKey((KeyCode)((int)KeyCode.Mouse0 + (int)button));
            }

            return false;
        }

        /// <summary>
        /// Returns true during the frame the user pressed the given mouse button.
        /// </summary>
        /// <returns><c>true</c>, if user pressed mouse button, <c>false</c> otherwise.</returns>
        /// <param name="button">Mouse button.</param>
        public static bool GetMouseButtonDown(int button)
        {
            if (button >= 0 && button < (int)MouseButton.None)
            {
                return GetMouseButtonDown((MouseButton)button);
            }

            return false;
        }

        /// <summary>
        /// Returns true during the frame the user pressed the given <see cref="MouseButton"/>.
        /// </summary>
        /// <returns><c>true</c>, if user pressed mouse button, <c>false</c> otherwise.</returns>
        /// <param name="button">Mouse button.</param>
        public static bool GetMouseButtonDown(MouseButton button)
        {
            if (button != MouseButton.None)
            {
                return UnityEngine.Input.GetKeyDown((KeyCode)((int)KeyCode.Mouse0 + (int)button));
            }

            return false;
        }

        /// <summary>
        /// Returns true during the frame the user releases the given mouse button.
        /// </summary>
        /// <returns><c>true</c>, if user releases mouse button, <c>false</c> otherwise.</returns>
        /// <param name="button">Mouse button.</param>
        public static bool GetMouseButtonUp(int button)
        {
            if (button >= 0 && button < (int)MouseButton.None)
            {
                return GetMouseButtonUp((MouseButton)button);
            }

            return false;
        }

        /// <summary>
        /// Returns true during the frame the user releases the given <see cref="MouseButton"/>.
        /// </summary>
        /// <returns><c>true</c>, if user releases mouse button, <c>false</c> otherwise.</returns>
        /// <param name="button">Mouse button.</param>
        public static bool GetMouseButtonUp(MouseButton button)
        {
            if (button != MouseButton.None)
            {
                return UnityEngine.Input.GetKeyUp((KeyCode)((int)KeyCode.Mouse0 + (int)button));
            }

            return false;
        }

        /// <summary>
        /// Returns object representing status of a specific touch. (Does not allocate temporary variables).
        /// </summary>
        /// <returns>Touch instance.</returns>
        /// <param name="index">Touch index.</param>
        public static Touch GetTouch(int index)
        {
            return UnityEngine.Input.GetTouch(index);
        }

        /// <summary>
        /// Returns default gyroscope.
        /// </summary>
        /// <value>Default gyroscope.</value>
        public static Gyroscope gyro
        {
            get
            {
                return UnityEngine.Input.gyro;
            }
        }

        /// <summary>
        /// Controls enabling and disabling of IME input composition.
        /// </summary>
        /// <value>IME composition mode.</value>
        public static IMECompositionMode imeCompositionMode
        {
            get
            {
                return UnityEngine.Input.imeCompositionMode;
            }

            set
            {
                UnityEngine.Input.imeCompositionMode = value;
            }
        }

        /// <summary>
        /// Gets a value indicating that the user have an IME keyboard input source selected.
        /// </summary>
        /// <value><c>true</c> if IME keyboard input source selected; otherwise, <c>false</c>.</value>
        public static bool imeIsSelected
        {
            get
            {
                return UnityEngine.Input.imeIsSelected;
            }
        }

        /// <summary>
        /// Returns the keyboard input entered this frame. (Read Only)
        /// </summary>
        /// <value>Keyboard input.</value>
        public static string inputString
        {
            get
            {
                return UnityEngine.Input.inputString;
            }
        }

        /// <summary>
        /// Returns device location (handheld devices only).
        /// </summary>
        /// <value>Handheld device location.</value>
        public static LocationService location
        {
            get
            {
                return UnityEngine.Input.location;
            }
        }

        /// <summary>
        /// Gets the current mouse position in pixel coordinates.
        /// </summary>
        /// <value>Current mouse position.</value>
        public static Vector3 mousePosition
        {
            get
            {
                return UnityEngine.Input.mousePosition;
            }
        }

        /// <summary>
        /// Gets a value indicating that mouse is present.
        /// </summary>
        /// <value><c>true</c> if mouse is present; otherwise, <c>false</c>.</value>
        public static bool mousePresent
        {
            get
            {
                return UnityEngine.Input.mousePresent;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating that the system handles multiple touches.
        /// </summary>
        /// <value><c>true</c> if system handles multiple touches; otherwise, <c>false</c>.</value>
        public static bool multiTouchEnabled
        {
            get
            {
                return UnityEngine.Input.multiTouchEnabled;
            }

            set
            {
                UnityEngine.Input.multiTouchEnabled = value;
            }
        }

        /// <summary>
        /// Resets all input. After ResetInputAxes all axes return to 0 and all buttons return to 0 for one frame.
        /// </summary>
        public static void ResetInputAxes()
        {
            UnityEngine.Input.ResetInputAxes();
        }

        /// <summary>
        /// Gets or sets a value indicating that mouse actions are simulated as touches.
        /// </summary>
        /// <value><c>true</c> if mouse actions are simulated as touches; otherwise, <c>false</c>.</value>
        public static bool simulateMouseWithTouches
        {
            get
            {
                return UnityEngine.Input.simulateMouseWithTouches;
            }

            set
            {
                UnityEngine.Input.simulateMouseWithTouches = value;
            }
        }

        /// <summary>
        /// Gets the number of touches. Guaranteed not to change throughout the frame.
        /// </summary>
        /// <value>Number of touches.</value>
        public static int touchCount
        {
            get
            {
                return UnityEngine.Input.touchCount;
            }
        }

        /// <summary>
        /// Returns list of objects representing status of all touches during last frame. (Read Only) (Allocates temporary variables).
        /// </summary>
        /// <value>List of touches.</value>
        public static Touch[] touches
        {
            get
            {
                return UnityEngine.Input.touches;
            }
        }

        /// <summary>
        /// Returns whether the device on which application is currently running supports touch input.
        /// </summary>
        /// <value><c>true</c> if touch supported; otherwise, <c>false</c>.</value>
        public static bool touchSupported
        {
            get
            {
                return UnityEngine.Input.touchSupported;
            }
        }
    }
}
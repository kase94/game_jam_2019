﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems.Input
{
    public class JoystickInputSO : CustomInputSO
    {
        [SerializeField]
        private JoystickInput JoystickInput;

        public override CustomInput GetInput()
        {
            return JoystickInput;
        }
    }
}
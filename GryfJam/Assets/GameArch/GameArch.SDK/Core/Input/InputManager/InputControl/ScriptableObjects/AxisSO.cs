﻿using System;
using UnityEditor;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [Serializable]
    public class AxisSO
    {
        public string Name;
        public string Negative;
        public string Positive;
        public bool Inverted;
    }

   
}
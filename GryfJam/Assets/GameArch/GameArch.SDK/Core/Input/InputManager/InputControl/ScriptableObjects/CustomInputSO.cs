﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems.Input
{ 
    public class CustomInputSO : ScriptableObject
    {
        public virtual CustomInput GetInput()
        {
            return null;
        }
    }
}
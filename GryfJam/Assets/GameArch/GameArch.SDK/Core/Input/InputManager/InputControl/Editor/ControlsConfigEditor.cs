﻿using GameArch.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ControlsConfig))]
    public class ControlsConfigEditor : Editor
    {
        ControlsConfig comp;
        string _newKeyName;

        public void OnEnable()
        {
            comp = (ControlsConfig)target;
            if (comp.KeyMappingSOList == null)
                comp.KeyMappingSOList = new List<KeyMappingSO>();

            if (comp.Axes == null)
                comp.Axes = new List<AxisSO>();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("Axes"), true);

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox, GUILayout.MaxWidth(280.0f));
            EditorGUILayout.LabelField("New KeyMapping Name:", GUILayout.Width(140f));
            _newKeyName = EditorGUILayout.TextField(_newKeyName, GUILayout.Width(200f));

            if (GUILayout.Button("Add", GUILayout.Width(80f)))
            {
                KeyMappingSO keyMapping = CreateInstance<KeyMappingSO>();
                comp.KeyMappingSOList.Add(keyMapping);
                keyMapping.Name = _newKeyName;
                keyMapping.Parent = comp;
                AssetDatabase.AddObjectToAsset(keyMapping, comp);
                AssetDatabase.SaveAssets();
            }
            EditorGUILayout.EndHorizontal();
      
            for (int i = 0; i < comp.KeyMappingSOList.Count; i++)
            {
                GUILayout.BeginHorizontal(EditorStyles.helpBox, GUILayout.MaxWidth(280.0f));
          
                EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.MaxWidth(200.0f));
                var editor = Editor.CreateEditor(comp.KeyMappingSOList[i]);
                if (editor != null)
                {
                    // editor.DrawDefaultInspectorWithoutScriptField();
                    editor.OnInspectorGUI();
                }

                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUILayout.MaxWidth(80.0f));

                if (GUILayout.Button("Remove"))
                {
                    KeyMappingSO keyMapping = comp.KeyMappingSOList[i];
                    comp.KeyMappingSOList.RemoveAt(i);
                    AssetDatabase.RemoveObjectFromAsset(keyMapping.PrimaryInput);
                    AssetDatabase.RemoveObjectFromAsset(keyMapping.SecondaryInput);
                    AssetDatabase.RemoveObjectFromAsset(keyMapping.ThirdInput);
                    AssetDatabase.RemoveObjectFromAsset(keyMapping);
                }
                 

                EditorGUILayout.EndVertical();

                GUILayout.EndHorizontal();
                GUILayout.Space(20);
            }
            GUILayout.FlexibleSpace();

            serializedObject.ApplyModifiedProperties();

        }
    }
   
}

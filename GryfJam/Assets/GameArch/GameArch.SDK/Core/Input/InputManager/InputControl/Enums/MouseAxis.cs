using System;
/// <summary>
/// Mouse axis. Enumeration for mouse axes.
/// </summary>
[Serializable]
public enum MouseAxis
{
      MouseLeft
    , MouseRight
    , MouseUp
    , MouseDown
    , WheelUp
    , WheelDown
    , None
}

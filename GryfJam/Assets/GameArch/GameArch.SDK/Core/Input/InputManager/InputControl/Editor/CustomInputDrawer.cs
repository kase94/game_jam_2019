﻿using UnityEditor;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [CustomPropertyDrawer(typeof(CustomInput), true)]
    public class CustomInputDrawer : PropertyDrawer
    {
        private SerializedProperty _mKey;
        private SerializedProperty _mModifiers;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var amountRect = new Rect(position.x, position.y, 150, position.height);
            var unitRect = new Rect(position.x + 155, position.y, 100, position.height);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //    EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("mKey"), GUIContent.none);
            EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("mModifiers"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}
﻿using System;
using UnityEditor;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [Serializable]
    [CreateAssetMenu(fileName = "KeyMappingSO", menuName = "GameArch/Input/KeyMappingSO", order = 5)]
    public class KeyMappingSO : ScriptableObject
    {
        public string Name;
        public CustomInputSO PrimaryInput;
        public CustomInputSO SecondaryInput;
        public CustomInputSO ThirdInput;
        public UnityEngine.Object Parent;

        public void OnEnable()
        {
         
            if (PrimaryInput == null)
                PrimaryInput = ScriptableObject.CreateInstance<CustomInputSO>();
            if (SecondaryInput == null)
                SecondaryInput = ScriptableObject.CreateInstance<CustomInputSO>();
            if (ThirdInput == null)
                ThirdInput = ScriptableObject.CreateInstance<CustomInputSO>();

        }
    }

   
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameArch.Systems.Input
{
    public class InputManager : Singleton<InputManager>
    {
        [SerializeField] InputConfig _config;
        [SerializeField] bool _isRecording;
        [SerializeField] protected State state;

        #region HelpStructures
        [Serializable]
        public class InputRecord
        {
            public InputRecord()
            {
                _recordedInput = new List<InputSnapShot>();
            }
            private List<InputSnapShot> _recordedInput;

            #region public API
            private Vector3 _mousePos;



            public bool IsEmpty()
            {
                return _recordedInput.Count > 0 ? false : true;
            }

            public void Clear()
            {
                _recordedInput.Clear();
            }

            public void Add(InputSnapShot snapShot)
            {
                _recordedInput.Add(snapShot);
            }

            public List<InputSnapShot> GetSnapShots()
            {
                return _recordedInput;
            }

            public Vector3 MousePosition
            {
                get
                {
                    var input = _recordedInput;
                    if (input != null && input.Count > 0)
                    {
                        var lastInput = _recordedInput.Last();
                        if (lastInput != null)
                        {
                            return lastInput.input.MousePosition;
                        }
                    }
                    return UnityEngine.Input.mousePosition;
                }

                private set
                {
                }
            }

            public float GetAxisRaw(string axisName)
            {
                var input = _recordedInput;
                if (input != null && input.Count > 0)
                {
                    var lastInput = _recordedInput.Last();
                    if (lastInput != null)
                    {
                        return lastInput.input.GetAxisRaw(axisName);
                    }
                }
                return 0;
            }

            public float GetAxis(string axisName)
            {
                var input = _recordedInput.Last();
                if (input != null)
                {
                    return input.input.GetAxis(axisName);
                }
                return 0;
            }

            public bool GetButtonDown(string buttonName)
            {
                foreach (var snapShot in _recordedInput)
                {
                    if (snapShot.input.GetButtonDown(buttonName))
                        return true;
                }
                return false;
            }

            public bool GetButtonUp(string buttonName)
            {
                foreach (var snapShot in _recordedInput)
                {
                    if (snapShot.input.GetButtonUp(buttonName))
                        return true;
                }
                return false;
            }

            public bool GetButton(string buttonName)
            {
                foreach (var snapShot in _recordedInput)
                {
                    if (snapShot.input.GetButton(buttonName))
                        return true;
                }
                return false;
            }
            #endregion
        }

        [Serializable]
        public class InputSnapShot
        {
            public InputSnapShot() { }
            public InputSnapShot(float time, InputSource inputSource)
            {
                Time = time;
                input = inputSource;
            }
            public readonly float Time;
            public readonly InputSource input;
        }
        [Serializable]
        public class InputConfig
        {
            public InputConfig() { }
            public InputConfig(List<string> buttons = null, List<string> keys = null, List<string> axies = null)
            {
                if (buttons != null)
                {
                    Buttons = buttons;
                }
                if (keys != null)
                {
                    Keys = keys;
                }
                if (axies != null)
                {
                    Axies = axies;
                }
            }
            public List<string> Buttons;
            public List<string> Keys;
            public List<string> Axies;
        }

        public class InputSource
        {
            public InputSource()
            {
                MousePosition = new Vector3();
                ButtonsDown = new List<string>();
                ButtonsUp = new List<string>();
                Buttons = new List<string>();
                KeysDown = new List<string>();
                KeysUp = new List<string>();
                Keys = new List<string>();
                Axies = new Dictionary<string, float>();
                AxiesRaw = new Dictionary<string, float>();
            }

            public Vector3 MousePosition { get; private set; }          
            public readonly List<string> ButtonsDown;
            public readonly List<string> ButtonsUp;
            public readonly List<string> Buttons;
            public readonly List<string> KeysDown;
            public readonly List<string> KeysUp;
            public readonly List<string> Keys;
            public readonly Dictionary<string, float> Axies;
            public readonly Dictionary<string, float> AxiesRaw;


            public void Record(InputConfig config)
            {
                MousePosition = UnityEngine.Input.mousePosition;
                // get buttons up, down and hold
                foreach (var button in config.Buttons ?? Enumerable.Empty<string>())
                {
                    if (UnityEngine.Input.GetButtonUp(button))
                        ButtonsUp.Add(button);

                    if (UnityEngine.Input.GetButtonDown(button))
                        ButtonsDown.Add(button);

                    if (UnityEngine.Input.GetButton(button))
                        Buttons.Add(button);
                }

                // get keys up, down and hold
                foreach (var button in config.Keys ?? Enumerable.Empty<string>())
                {
                    if (UnityEngine.Input.GetKeyUp(button))
                        KeysUp.Add(button);

                    if (UnityEngine.Input.GetKeyDown(button))
                        KeysDown.Add(button);

                    if (UnityEngine.Input.GetKey(button))
                        Keys.Add(button);
                }

                // get axies
                foreach (var axis in config.Axies ?? Enumerable.Empty<string>())
                {
                    Axies.Add(axis, UnityEngine.Input.GetAxis(axis));
                    AxiesRaw.Add(axis, UnityEngine.Input.GetAxisRaw(axis));
                }
            }

            public bool IsEmpty()
            {
                if (Count(ButtonsDown) > 0 || Count(Buttons) > 0 || Count(KeysDown) > 0 || Count(Keys) > 0 || AxisInput(AxiesRaw))
                    return false;
                return true;
            }

            private int Count<T>(List<T> list)
            {
                if (list != null)
                    return list.Count;
                return 0;
            }

            private bool AxisInput(Dictionary<string, float> dict)
            {
                if (dict != null && dict.Count > 0)          
                    return dict.Values.Max() != 0 || dict.Values.Min() != 0 ? true : false;
                return false;
            }

            public float GetAxisRaw(string axisName)
            {
                if (AxiesRaw == null) return 0;
                return AxiesRaw.ContainsKey(axisName) ? AxiesRaw[axisName] : 0;
            }

            public float GetAxis(string axisName)
            {
                if (Axies == null) return 0;
                return Axies.ContainsKey(axisName) ? Axies[axisName] : 0;
            }

            public bool GetButtonDown(string buttonName)
            {
                if (ButtonsDown == null) return false;
                return ButtonsDown.Contains(buttonName);
            }

            public bool GetButtonUp(string buttonName)
            {
                if (ButtonsUp == null) return false;
                return ButtonsUp.Contains(buttonName);
            }

            public bool GetButton(string buttonName)
            {
                if (Buttons == null) return false;
                return Buttons.Contains(buttonName);
            }

            public bool GetKeyDown(string buttonName)
            {
                if (KeysDown == null) return false;
                return KeysDown.Contains(buttonName);
            }

            public bool GetKeyUp(string buttonName)
            {
                if (KeysUp == null) return false;
                return KeysUp.Contains(buttonName);
            }

            public bool GetKey(string buttonName)
            {
                if (Keys == null) return false;
                return Keys.Contains(buttonName);
            }
        }

        public enum State
        {
            Record,
            Replay
        }

        #endregion

        private int _replayIndex;
        private float _replayStart;
        private float _recordStart;
        private float _timer;

        InputRecord _lastInput = new InputRecord();
        InputRecord _recordedInput = new InputRecord();

        private bool _clear;
        private int _fixedUpdateCount;

        public void Init(InputConfig config = null, bool record = false)
        {
            _isRecording = record;
            _lastInput = new InputRecord();
            _recordedInput = new InputRecord();
            if (config != null)
                _config = config;
            else
                _config = new InputConfig();
        }

        private void Awake()
        {
            switch (state)
            {
                case State.Record:
                    StartRecord();
                    break;
                case State.Replay:
                    StartReplay();
                    break;
                default:
                    break;
            }
        }

        void Update()
        {
            switch (state)
            {
                case State.Record:
                    RecordInput(_timer);
                    break;
                case State.Replay:
                    //ReplayInput();
                    break;
                default:
                    break;
            }
        }

        private void FixedUpdate()
        {
            if (state == State.Replay)
                ReplayInput();
            _clear = true;
            _timer += Time.fixedDeltaTime;
            if (_lastInput.IsEmpty() == false)
            {
                //Debug.Log("Fixed update:" + _fixedUpdateCount);                 
            }
            ++_fixedUpdateCount;
        }

        private void ReplayInput()
        {
            if (_clear)
            {
                _lastInput.Clear();
                _clear = false;

            }

            //Debug.Log("replay time: " + time);
            var recordedSnapshots = _recordedInput.GetSnapShots();
            for (int i = _replayIndex; i < recordedSnapshots.Count; i++)
            {
                if (MathHelper.IsEqual(recordedSnapshots[i].Time, _timer))
                {
                    _lastInput.Add(recordedSnapshots[i]);
                    Debug.Log("input replayed");
                }
                else
                {

                    if (_timer > recordedSnapshots[i].Time)
                    {
                        Debug.Log("Uncertainty:" + _timer + "past " + recordedSnapshots[i].Time);
                    }

                    _replayIndex = i;
                    break;
                }
            }
        }

        private void RecordInput(float time)
        {
            if (_clear)
            {
                _lastInput.Clear();
                _clear = false;
            }


            InputSource input = new InputSource();
            input.Record(_config);

            if (input.IsEmpty() == false)
            {
                var snapShot = new InputSnapShot(time - _recordStart, input);
                _lastInput.Add(snapShot);
                if (_isRecording)
                    _recordedInput.Add(snapShot);

                //Debug.Log("record time:" + snapShot.Time);
            }
        }

        public void StartReplay()
        {
            _timer = 0;
            _fixedUpdateCount = 0;
            _lastInput.Clear();
            _replayStart = 0;
            Debug.Log("_replayStart: " + _replayStart);
            _replayIndex = 0;
            state = State.Replay;
        }

        public void StartRecord()
        {
            _timer = 0;
            _fixedUpdateCount = 0;
            _recordStart = 0;
           // Debug.Log("_recordStart: " + _recordStart);
            state = State.Record;
        }

        public InputRecord GetLastInput()
        {
            return _lastInput;
        }
    }
}

﻿using GameArch.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameArch.Systems.Input
{
    [Serializable, CreateAssetMenu(fileName = "ControlsConfig", menuName = "GameArch/Input/Controls Config", order = 1)]
    public class ControlsConfig : ScriptableObject
    {
        [SerializeField] public List<AxisSO> Axes;
        [SerializeField] public List<KeyMappingSO> KeyMappingSOList;

        public void OnEnable()
        {
            if (KeyMappingSOList == null)
                KeyMappingSOList = new List<KeyMappingSO>();
            if (Axes == null)
                Axes = new List<AxisSO>();
        }

        public Controls Register(string name = "")
        {
            name = string.IsNullOrEmpty(name) ? this.name : name;

            var controls = InputControl.SetControls(name);

            foreach (var key in KeyMappingSOList)
            {
                controls.SetKey(key.Name, key.PrimaryInput.GetInput(), key.SecondaryInput.GetInput(), key.ThirdInput.GetInput());
            }

            foreach (var axis in Axes)
            {
                controls.SetAxis(axis.Name, axis.Negative, axis.Positive);
            }

            return controls;
        }

    }
}

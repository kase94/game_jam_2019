﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

namespace GameArch.Systems.Input
{
    public class ControlsRecorder : Singleton<ControlsRecorder>
    {
        [SerializeField] bool _isRecording;
        [SerializeField] protected State state;

        #region HelpStructures
        [Serializable]
        public class InputRecord
        {
            private List<InputSnapShot> _recordedInput;

            #region public API

            public InputRecord()
            {
                _recordedInput = new List<InputSnapShot>();
            }

            public bool IsEmpty()
            {
                return _recordedInput.Count > 0 ? false : true;
            }

            public void Clear()
            {
                _recordedInput.Clear();
            }

            public void Add(InputSnapShot snapShot)
            {
                _recordedInput.Add(snapShot);
            }

            public List<InputSnapShot> GetSnapShots()
            {
                return _recordedInput;
            }

            public Vector3 MousePosition
            {
                get
                {
                    var input = _recordedInput;
                    if (input != null && input.Count > 0)
                    {
                        var lastInput = _recordedInput.Last();
                        if (lastInput != null)
                        {
                            return lastInput.MousePosition;
                        }
                    }
                    return UnityEngine.Input.mousePosition;
                }

                private set
                {
                }
            }

            public float GetAxisRaw(string controlsName, string axisName, bool exactKeyModifiers = false)
            {
                if (_recordedInput != null && _recordedInput.Count > 0)
                {
                    var lastInput = _recordedInput.Last();
                    InputSource inputSource;
                    if (lastInput != null && lastInput.input.TryGetValue(controlsName, out inputSource))
                    {
                        return inputSource.GetAxisRaw(axisName, exactKeyModifiers);
                    }
                }
                return 0;
            }

            public float GetAxis(string controlsName, string axisName, bool exactKeyModifiers = false)
            {
                InputSource inputSource;
                if (_recordedInput != null && _recordedInput.Count > 0 && _recordedInput.Last().input.TryGetValue(controlsName, out inputSource))
                {
                    return inputSource.GetAxis(axisName, exactKeyModifiers);
                }
                return 0;
            }

            public bool GetButtonDown(string controlsName, string buttonName, bool exactKeyModifiers = false)
            {
                foreach (var snapShot in _recordedInput)
                {
                    InputSource inputSource;
                    if (snapShot.input.TryGetValue(controlsName, out inputSource))
                    {
                        if (inputSource.GetButtonDown(buttonName, exactKeyModifiers))
                            return true;
                    }
                }
                return false;
            }

            public bool GetButtonUp(string controlsName, string buttonName, bool exactKeyModifiers = false)
            {
                foreach (var snapShot in _recordedInput)
                {
                    InputSource inputSource;
                    if (snapShot.input.TryGetValue(controlsName, out inputSource))
                    {
                        if (inputSource.GetButtonUp(buttonName, exactKeyModifiers))
                            return true;
                    }
                }
                return false;
            }

            public bool GetButton(string controlsName, string buttonName, bool exactKeyModifiers = false)
            {
                foreach (var snapShot in _recordedInput)
                {
                    InputSource inputSource;
                    if (snapShot.input.TryGetValue(controlsName, out inputSource))
                    {
                        if (inputSource.GetButton(buttonName, exactKeyModifiers))
                            return true;
                    }
                }
                return false;
            }
            #endregion
        }

        [Serializable]
        public class InputSnapShot
        {
            public readonly float Time;
            public readonly Dictionary<string, InputSource> input;
            public Vector3 MousePosition { get; private set; }

            public InputSnapShot() { }

            public InputSnapShot(float time, Dictionary<string, InputSource> inputSource)
            {
                Time = time;
                input = inputSource;
            }

            public void RecordMouse()
            {
                MousePosition = UnityEngine.Input.mousePosition;
            }
        }

        public class InputSource
        {
            public readonly List<string> ButtonsDown;
            public readonly List<string> ButtonsUp;
            public readonly List<string> Buttons;

            public readonly List<string> ButtonsDownKeyMod;
            public readonly List<string> ButtonsUpKeyMod;
            public readonly List<string> ButtonsKeyMod;

            public readonly Dictionary<string, float> Axies;
            public readonly Dictionary<string, float> AxiesRaw;

            public readonly Dictionary<string, float> AxiesKeyMod;
            public readonly Dictionary<string, float> AxiesRawKeyMod;

            public InputSource()
            {
                //MousePosition = new Vector3();
                ButtonsDown = new List<string>();
                ButtonsUp = new List<string>();
                Buttons = new List<string>();

                ButtonsDownKeyMod = new List<string>();
                ButtonsUpKeyMod = new List<string>();
                ButtonsKeyMod = new List<string>();

                Axies = new Dictionary<string, float>();
                AxiesRaw = new Dictionary<string, float>();

                AxiesKeyMod = new Dictionary<string, float>();
                AxiesRawKeyMod = new Dictionary<string, float>();
            }

            public void Record(Controls controls)
            {
                // get buttons up, down and hold
                foreach (var button in controls.GetKeys() ?? Enumerable.Empty<KeyMapping>())
                {
                    if (controls.GetButtonUp(button))
                        ButtonsUp.Add(button.name);

                    if (controls.GetButtonDown(button))
                        ButtonsDown.Add(button.name);

                    if (controls.GetButton(button))
                        Buttons.Add(button.name);

                    if (controls.GetButtonUp(button, true))
                        ButtonsUpKeyMod.Add(button.name);

                    if (controls.GetButtonDown(button, true))
                        ButtonsDownKeyMod.Add(button.name);

                    if (controls.GetButton(button, true))
                        ButtonsKeyMod.Add(button.name);
                }

                // get axies
                foreach (var axis in controls.GetAxes() ?? Enumerable.Empty<Axis>())
                {
                    Axies.Add(axis.name, controls.GetAxis(axis));
                    AxiesRaw.Add(axis.name, controls.GetAxisRaw(axis));

                    AxiesKeyMod.Add(axis.name, controls.GetAxis(axis, true));
                    AxiesRawKeyMod.Add(axis.name, controls.GetAxisRaw(axis, true));
                }
            }

            public bool IsEmpty()
            {
                if (Count(ButtonsDown) > 0 || Count(Buttons) > 0  || AxisInput(AxiesRaw))
                    return false;
                return true;
            }

            private int Count<T>(List<T> list)
            {
                if (list != null)
                    return list.Count;
                return 0;
            }

            private bool AxisInput(Dictionary<string, float> dict)
            {
                if (dict != null && dict.Count > 0)          
                    return dict.Values.Max() != 0 || dict.Values.Min() != 0 ? true : false;
                return false;
            }

            public float GetAxisRaw(string axisName, bool exactKeyModifiers = false)
            {
                if (exactKeyModifiers)
                {
                    if (AxiesRawKeyMod == null) return 0;
                    return AxiesRawKeyMod.ContainsKey(axisName) ? AxiesRawKeyMod[axisName] : 0;
                }
                else
                {
                    if (AxiesRaw == null) return 0;
                    return AxiesRaw.ContainsKey(axisName) ? AxiesRaw[axisName] : 0;
                }
               
            }

            public float GetAxis(string axisName, bool exactKeyModifiers = false)
            {
                if (exactKeyModifiers)
                {
                    if (AxiesKeyMod == null) return 0;
                    return AxiesKeyMod.ContainsKey(axisName) ? AxiesKeyMod[axisName] : 0;
                }
                else
                {
                    if (Axies == null) return 0;
                    return Axies.ContainsKey(axisName) ? Axies[axisName] : 0;
                }
            }

            public bool GetButtonDown(string buttonName, bool exactKeyModifiers = false)
            {
                if (exactKeyModifiers)
                {
                    if (ButtonsDownKeyMod == null) return false;
                    return ButtonsDownKeyMod.Contains(buttonName);
                }
                else
                {
                    if (ButtonsDown == null) return false;
                    return ButtonsDown.Contains(buttonName);
                }
            }

            public bool GetButtonUp(string buttonName, bool exactKeyModifiers = false)
            {
                if (exactKeyModifiers)
                {
                    if (ButtonsUpKeyMod == null) return false;
                    return ButtonsUpKeyMod.Contains(buttonName);
                }
                else
                {
                    if (ButtonsUp == null) return false;
                    return ButtonsUp.Contains(buttonName);
                }
            }

            public bool GetButton(string buttonName, bool exactKeyModifiers = false)
            {
                if (exactKeyModifiers)
                {
                    if (ButtonsKeyMod == null) return false;
                    return ButtonsKeyMod.Contains(buttonName);
                }
                else
                {
                    if (Buttons == null) return false;
                    return Buttons.Contains(buttonName);
                }
            }
        }

        public enum State
        {
            Record,
            Replay
        }

        #endregion

        private int _replayIndex;
        private float _replayStart;
        private float _recordStart;
        private float _timer;

        InputRecord _lastInput = new InputRecord();
        InputRecord _recordedInput = new InputRecord();

        private bool _clear;
        private int _fixedUpdateCount;

        private void Awake()
        {
            switch (state)
            {
                case State.Record:
                    StartRecord();
                    break;
                case State.Replay:
                    StartReplay();
                    break;
                default:
                    break;
            }
        }

        void Update()
        {
            switch (state)
            {
                case State.Record:
                    RecordInput(_timer);
                    break;
                case State.Replay:
                    //ReplayInput();
                    break;
                default:
                    break;
            }
        }

        private void FixedUpdate()
        {
            if (state == State.Replay)
                ReplayInput();
            _clear = true;
            _timer += Time.fixedDeltaTime;
            if (_lastInput.IsEmpty() == false)
            {
                //Debug.Log("Fixed update:" + _fixedUpdateCount);                 
            }
            ++_fixedUpdateCount;
        }

        private void ReplayInput()
        {
            if (_clear)
            {
                _lastInput.Clear();
                _clear = false;

            }

            //Debug.Log("replay time: " + time);
            var recordedSnapshots = _recordedInput.GetSnapShots();
            for (int i = _replayIndex; i < recordedSnapshots.Count; i++)
            {
                if (MathHelper.IsEqual(recordedSnapshots[i].Time, _timer))
                {
                    _lastInput.Add(recordedSnapshots[i]);
                    Debug.Log("input replayed");
                }
                else
                {

                    if (_timer > recordedSnapshots[i].Time)
                    {
                        Debug.Log("Uncertainty:" + _timer + "past " + recordedSnapshots[i].Time);
                    }

                    _replayIndex = i;
                    break;
                }
            }
        }

        private void RecordInput(float time)
        {
            if (_clear)
            {
                _lastInput.Clear();
                _clear = false;
            }

            Dictionary<string, InputSource> recordedInput = new Dictionary<string, InputSource>();

            foreach (var controls in InputControl.GetControls() ?? Enumerable.Empty<Controls>())
            {
                InputSource input = new InputSource();
                input.Record(controls);
                recordedInput[controls.Name] = input;
            }

            if (recordedInput.Count > 0)
            {
                var snapShot = new InputSnapShot(time - _recordStart, recordedInput);
                snapShot.RecordMouse();
                _lastInput.Add(snapShot);

                if (_isRecording)
                    _recordedInput.Add(snapShot);

                //Debug.Log("record time:" + snapShot.Time);
            }
        }

        public void StartReplay()
        {
            _timer = 0;
            _fixedUpdateCount = 0;
            _lastInput.Clear();
            _replayStart = 0;
            Debug.Log("_replayStart: " + _replayStart);
            _replayIndex = 0;
            state = State.Replay;
        }

        public void StartRecord()
        {
            _timer = 0;
            _fixedUpdateCount = 0;
            _recordStart = 0;
           // Debug.Log("_recordStart: " + _recordStart);
            state = State.Record;
        }

        public InputRecord GetLastInput()
        {
            return _lastInput;
        }
    }
}

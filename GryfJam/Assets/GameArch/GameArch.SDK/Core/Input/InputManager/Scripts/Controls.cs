﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace GameArch.Systems.Input
{
    public class Controls
    {
        #region Private Fields
        public const float NO_SMOOTH = 1000f;

        private string _name;
        // Set of keys
        List<KeyMapping> _keysList = new List<KeyMapping>();
        Dictionary<string, KeyMapping> sKeysMap = new Dictionary<string, KeyMapping>();
        // Set of axes
        List<Axis> _AxesList = new List<Axis>();
        Dictionary<string, Axis> sAxesMap = new Dictionary<string, Axis>();
        // Smooth for GetAxis
        private readonly Dictionary<string, float> sSmoothAxesValues = new Dictionary<string, float>();
        private float sSmoothCoefficient = 5f; // Smooth looks the same as in Input.GetAxis() with this value

        // Common options
        private InputDevice _inputDevice = InputDevice.Any;
        
        #endregion

        #region Properties
        public string Name { get => _name; private set => _name = value; }
        public Dictionary<string, KeyMapping> Keys { get => sKeysMap; set => sKeysMap = value; }
        public Dictionary<string, Axis> Axes { get => sAxesMap; set => sAxesMap = value; }


        #region smoothCoefficient
        /// <summary>
        /// Gets or sets the axis smooth coefficient. Smooth coefficient is used in GetAxis method to make the movement a little smoothed as well as in Input.GetAxis
        /// </summary>
        /// <value>Axis smooth coefficient.</value>
        public float smoothCoefficient
        {
            get
            {
                return sSmoothCoefficient;
            }

            set
            {
                if (value < 0.0001f)
                {
                    sSmoothCoefficient = 0.0001f;
                }
                else
                if (value > NO_SMOOTH)
                {
                    sSmoothCoefficient = NO_SMOOTH;
                }
                else
                {
                    sSmoothCoefficient = value;
                }
            }
        }
        #endregion

        #endregion

        #region Setup keys

        #region SetKey with different arguments

        #region Level 1
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        public KeyMapping SetKey(string name, KeyCode primary)
        {
            return SetKey(name, ArgToInput(primary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary)
        {
            return SetKey(name, ArgToInput(primary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        public KeyMapping SetKey(string name, MouseButton primary)
        {
            return SetKey(name, ArgToInput(primary));
        }
        #endregion

        // ============================================================================================================

        #region Level 2

        #region Level 2-0
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, KeyCode secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseAxis secondary)
        { 
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseButton secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 2-1
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, CustomInput secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, KeyCode secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseAxis secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseButton secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 2-2
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, CustomInput secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, KeyCode secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseAxis secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseButton secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 2-3
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, CustomInput secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, KeyCode secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseAxis secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseButton secondary)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary));
        }
        #endregion

        #endregion

        // ============================================================================================================

        #region Level 3

        #region Level 3-0

        #region Level 3-0-0
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, CustomInput secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, CustomInput secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, CustomInput secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-0-1
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, KeyCode secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, KeyCode secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, KeyCode secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, KeyCode secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-0-2
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseAxis secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseAxis secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseAxis secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseAxis secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-0-3
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseButton secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseButton secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseButton secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary, MouseButton secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        #endregion

        // ************************************************************************************************************

        #region Level 3-1

        #region Level 3-1-0
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, CustomInput secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, CustomInput secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, CustomInput secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, CustomInput secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-1-1
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, KeyCode secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, KeyCode secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, KeyCode secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, KeyCode secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-1-2
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseAxis secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseAxis secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseAxis secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseAxis secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-1-3
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseButton secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseButton secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseButton secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, KeyCode primary, MouseButton secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        #endregion

        // ************************************************************************************************************

        #region Level 3-2

        #region Level 3-2-0
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, CustomInput secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, CustomInput secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, CustomInput secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, CustomInput secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-2-1
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, KeyCode secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, KeyCode secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, KeyCode secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, KeyCode secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-2-2
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseAxis secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseAxis secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseAxis secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseAxis secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-2-3
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseButton secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseButton secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseButton secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseAxis primary, MouseButton secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        #endregion

        // ************************************************************************************************************

        #region Level 3-3

        #region Level 3-3-0
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, CustomInput secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, CustomInput secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, CustomInput secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, CustomInput secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-3-1
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, KeyCode secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, KeyCode secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, KeyCode secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, KeyCode secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-3-2
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseAxis secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseAxis secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseAxis secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseAxis secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region Level 3-3-3
        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseButton secondary, CustomInput third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseButton secondary, KeyCode third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseButton secondary, MouseAxis third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, MouseButton primary, MouseButton secondary, MouseButton third)
        {
            return SetKey(name, ArgToInput(primary), ArgToInput(secondary), ArgToInput(third));
        }
        #endregion

        #endregion

        #endregion

        // ============================================================================================================

        #region Argument to Input fuctions
        /// <summary>
        /// Convert argument to <see cref="CustomInput"/>.
        /// </summary>
        /// <returns>Converted CustomInput.</returns>
        /// <param name="arg">Some kind of argument.</param>
        private CustomInput ArgToInput(CustomInput arg)
        {
            return arg;
        }

        /// <summary>
        /// Convert argument to <see cref="CustomInput"/>.
        /// </summary>
        /// <returns>Converted CustomInput.</returns>
        /// <param name="arg">Some kind of argument.</param>
        private CustomInput ArgToInput(KeyCode arg)
        {
            return new KeyboardInput(arg);
        }

        /// <summary>
        /// Convert argument to <see cref="CustomInput"/>.
        /// </summary>
        /// <returns>Converted CustomInput.</returns>
        /// <param name="arg">Some kind of argument.</param>
        private CustomInput ArgToInput(MouseAxis arg)
        {
            return new MouseInput(arg);
        }

        /// <summary>
        /// Convert argument to <see cref="CustomInput"/>.
        /// </summary>
        /// <returns>Converted CustomInput.</returns>
        /// <param name="arg">Some kind of argument.</param>
        private CustomInput ArgToInput(MouseButton arg)
        {
            return new MouseInput(arg);
        }
        #endregion

        #endregion

        /// <summary>
        /// Create new <see cref="KeyMapping"/> with specified name and inputs.
        /// </summary>
        /// <returns>Created KeyMapping.</returns>
        /// <param name="name">KeyMapping name.</param>
        /// <param name="primary">Primary input.</param>
        /// <param name="secondary">Secondary input.</param>
        /// <param name="third">Third input.</param>
        public KeyMapping SetKey(string name, CustomInput primary = null, CustomInput secondary = null, CustomInput third = null)
        {
            KeyMapping outKey = null;

            if (sKeysMap.TryGetValue(name, out outKey))
            {
                outKey.primaryInput = primary;
                outKey.secondaryInput = secondary;
                outKey.thirdInput = third;
            }
            else
            {
                outKey = new KeyMapping(name, primary, secondary, third);

                _keysList.Add(outKey);
                sKeysMap.Add(name, outKey);
            }

            return outKey;
        }

        /// <summary>
        /// Removes <see cref="KeyMapping"/> by name.
        /// </summary>
        /// <param name="name">KeyMapping name.</param>
        public void RemoveKey(string name)
        {
            KeyMapping outKey = null;

            if (sKeysMap.TryGetValue(name, out outKey))
            {
                RemoveKey(outKey);
            }
        }

        /// <summary>
        /// Removes specified <see cref="KeyMapping"/>.
        /// </summary>
        /// <param name="key">KeyMapping instance.</param>
        public void RemoveKey(KeyMapping key)
        {
            bool good = true;

            if (!_keysList.Remove(key))
            {
                good = false;
            }

            if (!sKeysMap.Remove(key.name))
            {
                good = false;
            }

            if (!good)
            {
                Debug.LogError("Failed to remove key \"" + key.name + "\"");
            }
        }

        /// <summary>
        /// Gets <see cref="KeyMapping"/> by name.
        /// </summary>
        /// <param name="name">KeyMapping name.</param>
        public KeyMapping GetKeyByName(string name)
        {
            KeyMapping outKey = null;

            if (sKeysMap.TryGetValue(name, out outKey))
            {
                return outKey;
            }

            return null;
        }

        /// <summary>
        /// Gets the list of keys.
        /// </summary>
        /// <returns>List of keys.</returns>
        public ReadOnlyCollection<KeyMapping> GetKeys()
        {
            return _keysList.AsReadOnly();
        }
        #endregion

        #region Setup axes
        /// <summary>
        /// Create new <see cref="Axis"/> with specified negative <see cref="KeyMapping"/> and positive <see cref="KeyMapping"/>.
        /// </summary>
        /// <returns>Created Axis.</returns>
        /// <param name="name">Axis name.</param>
        /// <param name="negative">Name of negative KeyMapping.</param>
        /// <param name="positive">Name of positive KeyMapping.</param>
        public Axis SetAxis(string name, string negative, string positive)
        {
            KeyMapping negativeKey = null;
            KeyMapping positiveKey = null;

            if (!sKeysMap.TryGetValue(negative, out negativeKey))
            {
                Debug.LogError("Negative key \"" + negative + "\" not found for axis " + name);

                return null;
            }

            if (!sKeysMap.TryGetValue(positive, out positiveKey))
            {
                Debug.LogError("Positive key \"" + positive + "\" not found for axis " + name);

                return null;
            }

            return SetAxis(name, negativeKey, positiveKey);
        }

        /// <summary>
        /// Create new <see cref="Axis"/> with specified negative <see cref="KeyMapping"/> and positive <see cref="KeyMapping"/>.
        /// </summary>
        /// <returns>Created Axis.</returns>
        /// <param name="name">Axis name.</param>
        /// <param name="negative">Negative KeyMapping.</param>
        /// <param name="positive">Positive KeyMapping.</param>
        public Axis SetAxis(string name, KeyMapping negative, KeyMapping positive)
        {
            Axis outAxis = null;

            if (sAxesMap.TryGetValue(name, out outAxis))
            {
                outAxis.Set(negative, positive);
            }
            else
            {
                outAxis = new Axis(name, negative, positive);

                _AxesList.Add(outAxis);
                sAxesMap.Add(name, outAxis);
            }

            return outAxis;
        }

        /// <summary>
        /// Removes <see cref="Axis"/> by name.
        /// </summary>
        /// <param name="name">Axis name.</param>
        public void RemoveAxis(string name)
        {
            Axis outAxis = null;

            if (sAxesMap.TryGetValue(name, out outAxis))
            {
                RemoveAxis(outAxis);
            }
        }

        /// <summary>
        /// Removes specified <see cref="Axis"/>.
        /// </summary>
        /// <param name="axis">Axis instance.</param>
        public void RemoveAxis(Axis axis)
        {
            bool good = true;

            if (!_AxesList.Remove(axis))
            {
                good = false;
            }

            if (!sAxesMap.Remove(axis.name))
            {
                good = false;
            }

            if (!good)
            {
                Debug.LogError("Failed to remove axis \"" + axis.name + "\"");
            }
        }

        /// <summary>
        /// Gets <see cref="Axis"/> by name.
        /// </summary>
        /// <param name="name">Axis name.</param>
        public Axis GetAxisByName(string name)
        {
            Axis outAxis = null;

            if (sAxesMap.TryGetValue(name, out outAxis))
            {
                return outAxis;
            }

            return null;
        }

        /// <summary>
        /// Gets the list of axes.
        /// </summary>
        /// <returns>List of axes.</returns>
        public ReadOnlyCollection<Axis> GetAxes()
        {
            return _AxesList.AsReadOnly();
        }
        #endregion

        public Controls(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Returns the value of the virtual axis identified by axisName.
        /// </summary>
        /// <returns>Value of the virtual axis.</returns>
        /// <param name="axisName">Axis name.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public float GetAxis(string axisName, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                return ControlsRecorder.Instance.GetLastInput().GetAxis(Name, axisName, exactKeyModifiers);
            }
            else
            {
                float previousValue;

                if (!sSmoothAxesValues.TryGetValue(axisName, out previousValue))
                {
                    previousValue = 0f;
                }

                float totalCoefficient = sSmoothCoefficient * Time.deltaTime;

                if (totalCoefficient > 1)
                {
                    totalCoefficient = 1;
                }

                float newValue = GetAxisRaw(axisName, exactKeyModifiers);
                float res = previousValue + (newValue - previousValue) * totalCoefficient;

                sSmoothAxesValues[axisName] = res;

                return res;
            }
        }

        /// <summary>
        /// Returns the value of the virtual axis.
        /// </summary>
        /// <returns>Value of the virtual axis.</returns>
        /// <param name="axis">Axis instance.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public float GetAxis(Axis axis, bool exactKeyModifiers = false)
        {
            return GetAxis(axis.name, exactKeyModifiers);
        }

        /// <summary>
        /// Returns the value of the virtual axis identified by axisName with no smoothing filtering applied.
        /// </summary>
        /// <returns>Value of the virtual axis.</returns>
        /// <param name="axisName">Axis name.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public float GetAxisRaw(string axisName, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                return ControlsRecorder.Instance.GetLastInput().GetAxisRaw(Name, axisName, exactKeyModifiers);
            }
            else
            {
                float sensitivity = 1f;

                #region Standard axes
                if (axisName.Equals("Mouse X"))
                {
                    sensitivity = InputControl.mouseSensitivity;
                }
                else
                if (axisName.Equals("Mouse Y"))
                {
                    if (InputControl.invertMouseY)
                    {
                        sensitivity = -InputControl.mouseSensitivity;
                    }
                    else
                    {
                        sensitivity = InputControl.mouseSensitivity;
                    }
                }
                #endregion

                Axis outAxis = null;

                if (!sAxesMap.TryGetValue(axisName, out outAxis))
                {
                    if (
                        !axisName.Equals("Mouse X")
                        &&
                        !axisName.Equals("Mouse Y")
                        &&
                        !axisName.Equals("Mouse ScrollWheel")
                       )
                    {
                        Debug.LogError("Axis \"" + axisName + "\" not found. Using InputManager axis");
                    }

                    return UnityEngine.Input.GetAxisRaw(axisName) * sensitivity;
                }

                return outAxis.GetValue(exactKeyModifiers, _inputDevice) * sensitivity;
            }
        }

        /// <summary>
        /// Returns the value of the virtual axis with no smoothing filtering applied.
        /// </summary>
        /// <returns>Value of the virtual axis.</returns>
        /// <param name="axis">Axis instance.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public float GetAxisRaw(Axis axis, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                return ControlsRecorder.Instance.GetLastInput().GetAxisRaw(Name, axis.name, exactKeyModifiers);
            }
            else
            {
                float sensitivity = 1f;

                #region Standard axes
                if (axis.name.Equals("Mouse X"))
                {
                    sensitivity = InputControl.mouseSensitivity;
                }
                else
                if (axis.name.Equals("Mouse Y"))
                {
                    if (InputControl.invertMouseY)
                    {
                        sensitivity = -InputControl.mouseSensitivity;
                    }
                    else
                    {
                        sensitivity = InputControl.mouseSensitivity;
                    }
                }
                #endregion

                return axis.GetValue(exactKeyModifiers, _inputDevice) * sensitivity;
            }
        }

        /// <summary>
        /// Returns true while the virtual button identified by buttonName is held down.
        /// </summary>
        /// <returns><c>true</c>, if button is held down, <c>false</c> otherwise.</returns>
        /// <param name="buttonName">Button name.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public bool GetButton(string buttonName, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                Debug.Log("Fixed shit");
                return ControlsRecorder.Instance.GetLastInput().GetButton(Name, buttonName, exactKeyModifiers);
            }
            else
            {
                KeyMapping outKey = null;

                if (!sKeysMap.TryGetValue(buttonName, out outKey))
                {
                    Debug.LogError("Key \"" + buttonName + "\" not found");
                    return false;
                }

                return outKey.IsPressed(exactKeyModifiers, _inputDevice);
            }
        }

        /// <summary>
        /// Returns true while the virtual button is held down.
        /// </summary>
        /// <returns><c>true</c>, if button is held down, <c>false</c> otherwise.</returns>
        /// <param name="button">KeyMapping instance.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public bool GetButton(KeyMapping button, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                Debug.Log("Fixed shit");
                return ControlsRecorder.Instance.GetLastInput().GetButton(Name, button.name, exactKeyModifiers);
            }
            else
            {
                return button.IsPressed(exactKeyModifiers, _inputDevice);
            }
        }

        /// <summary>
        /// Returns true during the frame the user pressed down the virtual button identified by buttonName.
        /// </summary>
        /// <returns><c>true</c>, if user pressed down the button during the frame, <c>false</c> otherwise.</returns>
        /// <param name="buttonName">Button name.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public bool GetButtonDown(string buttonName, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                Debug.Log("Fixed shit");
                return ControlsRecorder.Instance.GetLastInput().GetButtonDown(Name, buttonName, exactKeyModifiers);
            }
            else
            {
                KeyMapping outKey = null;

                if (!sKeysMap.TryGetValue(buttonName, out outKey))
                {
                    Debug.LogError("Key \"" + buttonName + "\" not found");
                    return false;
                }

                return outKey.IsPressedDown(exactKeyModifiers, _inputDevice);
            }
        }

        /// <summary>
        /// Returns true during the frame the user pressed down the virtual button.
        /// </summary>
        /// <returns><c>true</c>, if user pressed down the button during the frame, <c>false</c> otherwise.</returns>
        /// <param name="button">KeyMapping instance.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public bool GetButtonDown(KeyMapping button, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                Debug.Log("Fixed shit");
                return ControlsRecorder.Instance.GetLastInput().GetButtonDown(Name, button.name, exactKeyModifiers);
            }
            else
            {
                return button.IsPressedDown(exactKeyModifiers, _inputDevice);
            }
        }

        /// <summary>
        /// Returns true the first frame the user releases the virtual button identified by buttonName.
        /// </summary>
        /// <returns><c>true</c>, if user releases the button during the frame, <c>false</c> otherwise.</returns>
        /// <param name="buttonName">Button name.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public bool GetButtonUp(string buttonName, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                Debug.Log("Fixed shit");
                return ControlsRecorder.Instance.GetLastInput().GetButtonUp(Name, buttonName, exactKeyModifiers);
            }
            else
            {
                KeyMapping outKey = null;

                if (!sKeysMap.TryGetValue(buttonName, out outKey))
                {
                    Debug.LogError("Key \"" + buttonName + "\" not found");
                    return false;
                }

                return outKey.IsPressedUp(exactKeyModifiers, _inputDevice);
            }
        }

        /// <summary>
        /// Returns true the first frame the user releases the virtual button.
        /// </summary>
        /// <returns><c>true</c>, if user releases the button during the frame, <c>false</c> otherwise.</returns>
        //// <param name="button">KeyMapping instance.</param>
        /// <param name="exactKeyModifiers">If set to <c>true</c> check that only specified key modifiers are active, otherwise check that at least specified key modifiers are active.</param>
        public bool GetButtonUp(KeyMapping button, bool exactKeyModifiers = false)
        {
            if (Time.inFixedTimeStep)
            {
                Debug.Log("Fixed shit");
                return ControlsRecorder.Instance.GetLastInput().GetButtonUp(Name, button.name, exactKeyModifiers);
            }
            else
            {
                return button.IsPressedUp(exactKeyModifiers, _inputDevice);
            }
        }

        /// <summary>
        /// Converts string representation of CustomInput to CustomInput.
        /// </summary>
        /// <returns>CustomInput from string.</returns>
        /// <param name="value">String representation of CustomInput.</param>
        public static CustomInput CustomInputFromString(string value)
        {
            CustomInput res;

            res = JoystickInput.FromString(value);

            if (res != null)
            {
                return res;
            }

            res = MouseInput.FromString(value);

            if (res != null)
            {
                return res;
            }

            res = KeyboardInput.FromString(value);

            if (res != null)
            {
                return res;
            }

            return null;
        }
    }
}

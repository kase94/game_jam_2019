﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Graph<Location>
{
    // NameValueCollection would be a reasonable alternative here, if
    // you're always using string location types
    public Dictionary<Location, Location[]> edges
        = new Dictionary<Location, Location[]>();

    public Location[] Neighbors(Location id)
    {
        return edges[id];
    }
};

class BreadthFirstSearch
{
    public static void Search(Graph<string> graph, string start)
    {
        var frontier = new Queue<string>();
        frontier.Enqueue(start);

        var visited = new HashSet<string>();
        visited.Add(start);

        while (frontier.Count > 0)
        {
            var current = frontier.Dequeue();

            Debug.Log($"Visiting {current}");
            foreach (var next in graph.Neighbors(current))
            {
                if (!visited.Contains(next))
                {
                    frontier.Enqueue(next);
                    visited.Add(next);
                }
            }
        }
    }
}
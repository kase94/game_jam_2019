﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    public Vector2 GridSize;
    public float NodeDistance = 0.05f;
    public float NodeRadius = 0.5f;
    public Vector2 StartPos;
    public LayerMask NotWalkable;
    public int[][] Grid;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmosSelected()
    {

#if UNITY_EDITOR
        //Gizmos.color = Color.red;
        Vector2 nodePos = new Vector2(0,0);
        for (int x = 0; x < GridSize.x; x++)
        {
            for (int y = 0; y < GridSize.y; y++)
            {
                nodePos.x = StartPos.x + x * NodeDistance;
                nodePos.y = StartPos.y + y * NodeDistance;
                if (Physics.CheckSphere(new Vector3(nodePos.x, nodePos.y, 0), NodeRadius, NotWalkable))
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawCube(nodePos, Vector3.one * NodeRadius * 2);
                }
                else
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawWireCube(nodePos, Vector3.one * NodeRadius * 2);
                }


            }
        }
#endif
    }
}

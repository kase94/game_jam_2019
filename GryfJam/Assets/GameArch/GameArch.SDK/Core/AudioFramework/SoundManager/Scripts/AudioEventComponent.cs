﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioEventComponent : MonoBehaviour {
    [Serializable]
    public class AudioEventSource
    {
        public AudioEvent audioEvent;
        private AudioSource _audioSource;

        public void SetSource(AudioSource audioSource)
        {
            _audioSource = audioSource;
        }

        public void Play()
        {
            audioEvent.Play(_audioSource);
        }
    };

    public AudioEventSource[] audioEvents;

    private void Awake()
    {
        foreach (var audio in audioEvents ?? Enumerable.Empty<AudioEventSource>())
        {
            audio.SetSource(gameObject.AddComponent<AudioSource>());
            Debug.Log(audio.audioEvent.name);
        }
    }

    public void Play(string name)
    {
        AudioEventSource audioEventSource = null;
        foreach (var audio in audioEvents ?? Enumerable.Empty<AudioEventSource>())
        {
            if(name == audio.audioEvent.name)
            {
                audioEventSource = audio;  audio.Play();
                break;
            }        
        }

        if (audioEventSource != null)
        {
            audioEventSource.Play();
        }
        else
        {
            Debug.LogWarning("AudioEventComponent - Cant find audio: " + name);
        }
    }
}

﻿namespace GameArch.Systems
{
    public interface IIdentity
    {
        Identity GetIdentity();
    }
}

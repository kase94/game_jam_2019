﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameArch.Systems.Identity;

namespace GameArch.Systems
{
    using EventKey = Tuple<Identity, string>;
    using EventQueKey = Tuple<Tuple<Identity, string>, ActionParams>;

    public class EventManager : Singleton<EventManager>
    {
        #region Fields
        protected Dictionary<EventKey, Action<ActionParams>> _events = new Dictionary<EventKey, Action<ActionParams>>(new IdentityEqualityComparer());
        protected Queue<EventQueKey> _fixedUpdateEventsQue = new Queue<EventQueKey>();
        #endregion

        #region Public API
        public void AddListener(Identity identity, string eventName, Action<ActionParams> action)
        {
            AddIdentityListener(identity, eventName, action);
        }

        public void StopListening(Identity identity, string eventName, Action<ActionParams> action)
        {
            StopIdentityListening(Identity.By.Name(identity.name), eventName, action);
            StopIdentityListening(Identity.By.ID(identity.id), eventName, action);
            StopIdentityListening(Identity.By.Player(identity.player), eventName, action);
            StopIdentityListening(Identity.By.Any(), eventName, action);
        }

        public void TriggerEvent(Identity identity, string eventName, ActionParams actionParams, bool invokeDuringFixedUpdate = true)
        {
            InvokeIdentity(Identity.By.Name(identity.name), eventName, actionParams);
            InvokeIdentity(Identity.By.ID(identity.id), eventName, actionParams);
            InvokeIdentity(Identity.By.Player(identity.player), eventName, actionParams);
            InvokeIdentity(Identity.By.Any(), eventName, actionParams);
        }
        #endregion

        #region Methods

        private void FixedUpdate()
        {
            EventQueKey queEvent;
            while (_fixedUpdateEventsQue.Count > 0)
            {
                queEvent = _fixedUpdateEventsQue.Dequeue();
                TriggerEvent(queEvent.Item1.Item1, queEvent.Item1.Item2, queEvent.Item2);
            }

        }

        protected void InvokeIdentity(Identity identity, string eventName, ActionParams actionParams)
        {
            if (identity != null)
            {
                Action<ActionParams> regAction = null;
                EventKey key = new EventKey(identity, eventName);
                if (_events.TryGetValue(key, out regAction))
                {
                    if (Time.inFixedTimeStep)
                    {
                        regAction.Invoke(actionParams);
                    }
                    else
                    {
                        _fixedUpdateEventsQue.Enqueue(new EventQueKey(key, actionParams));
                    }
                }
            }
        }

        protected void AddIdentityListener(Identity identity, string eventName, Action<ActionParams> action)
        {
            if (identity != null)
            {
                Action<ActionParams> regAction = null;
                EventKey key = new EventKey(identity, eventName);

                if (_events.TryGetValue(key, out regAction))
                {
                    regAction += action;
                }
                else
                {
                    regAction += action;
                    _events.Add(key, regAction);
                }
            }
        }

        protected void StopIdentityListening(Identity identity, string eventName, Action<ActionParams> action)
        {
            if(identity != null)
            {
                Action<ActionParams> regAction = null;
                EventKey key = new EventKey(identity, eventName);

                if (_events.TryGetValue(key, out regAction))
                {
                    regAction -= action;
                }
            }
        }
        #endregion
    }
}

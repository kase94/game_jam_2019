﻿using System.Collections;
using System.Collections.Generic;

namespace GameArch.Systems
{
    public class ActionParams : Dictionary<string, object>
    {
        public ActionParams Add<T>(string paramName, T value)
        {
            this[paramName] = value;
            return this;
        }

        public T As<T>( string paramName, T defaultValue = default(T))
        {
            return this == null || string.IsNullOrEmpty(paramName) || !this.ContainsKey(paramName)
                ? defaultValue
                : (T)this[paramName];
        }
    }
}

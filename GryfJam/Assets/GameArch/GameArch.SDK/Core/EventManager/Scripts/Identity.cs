﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems
{
    public class Identity
    {
        public string name;
        public string player;
        public string id;
        public bool any = false;

        public class By
        {
            public static Identity Name(string name)
            {
                if (name != null && name != "")
                {
                    return new Identity()
                    {
                        name = name
                    };
                }
                else
                {
                    return null;
                }
            }

            public static Identity Player(string player)
            {
                if (player != null && player != "")
                {
                    return new Identity()
                    {
                        player = player
                    };
                }
                else
                {
                    return null;
                }
            }

            public static Identity ID(string id)
            {
                if (id != null && id != "")
                {
                    return new Identity()
                    {
                        id = id
                    };
                }
                else
                {
                    return null;
                }

            }

            public static Identity Any()
            {
                return new Identity()
                {
                    any = true
                };
            }

        }

        public class IdentityEqualityComparer : IEqualityComparer<Tuple<Identity, string>>
        {
            public bool Equals(Tuple<Identity, string> x, Tuple<Identity, string> y)
            {
                return ( ((x.Item1.name == y.Item1.name) || (x.Item1.player == y.Item1.player) || (x.Item1.id == y.Item1.id) || (x.Item1.any == true && y.Item1.any == true)) && (x.Item2 == x.Item2));
            }

            public int GetHashCode(Tuple<Identity, string> obj)
            {
                string combined = obj.Item1.name + "|" + obj.Item1.player + "|" + obj.Item1.id + "|" + obj.Item1.any + "|" + obj.Item2;
                return (combined.GetHashCode());
            }
        }
    }
}
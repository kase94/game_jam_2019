﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Systems
{
    public class IdentityComponent : MonoBehaviour
    {
        public Identity identity = new Identity();

        void Awake()
        {
            identity.id = this.GetInstanceID().ToString();
        }

    }
}

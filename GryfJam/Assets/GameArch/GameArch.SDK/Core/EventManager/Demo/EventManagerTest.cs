﻿using GameArch.Systems;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManagerTest : MonoBehaviour
{
    
    private void Start()
    {
        EventManager.Instance.AddListener(Identity.By.Name("INPUT"), "Fire1", (actionParams) =>
        {
            Debug.Log("Fire1 pressed down");
            Debug.Log("actionParams bool" + actionParams.As<bool>("trigerred"));
            Debug.Log("actionParams integer" + actionParams.As<int>("integer"));
            Debug.Log("actionParams string" + actionParams.As<string>("string") );
          
        });
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            ActionParams actionParams = new ActionParams();
            actionParams.Add<bool>("triggered", true);
            actionParams.Add<int>("integer", 12);
            actionParams.Add<string>("string", "true story");

            EventManager.Instance.TriggerEvent(Identity.By.Name("INPUT"), "Fire1", actionParams);
        }
    }
}

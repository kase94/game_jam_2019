﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MenuComponent
{
    public class PauseMenu : MonoBehaviour
    {

        public UnityEvent OnResume;
        public UnityEvent OnRestart;
        public UnityEvent OnExit;
        public UnityEvent OnPause;

        public GameObject Content;

        protected bool _isPaused = false;

        public void OnResumeButton()
        {
            _isPaused = false;
            Content.SetActive(false);
            OnResume?.Invoke();
        }

        public void OnRestartButton()
        {
            _isPaused = false;
            Content.SetActive(false);
            OnRestart?.Invoke();
        }

        public void OnExitButton()
        {
            _isPaused = false;
            Content.SetActive(false);
            OnExit?.Invoke();
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif

        }

        public void Pause()
        {
            OnPause?.Invoke();
            _isPaused = true;
            Content.SetActive(true);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (_isPaused == false)
                {
                    Pause();
                }
                else
                {
                    OnResumeButton();
                }
            }
        }
    }
}

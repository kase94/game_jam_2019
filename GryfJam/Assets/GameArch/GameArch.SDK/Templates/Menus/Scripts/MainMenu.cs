﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MenuComponent
{
    public class MainMenu : MonoBehaviour
    {
        public string GameSceneName;

        public void OnPlayButton()
        {
            SceneManager.LoadScene(GameSceneName, LoadSceneMode.Single);
        }

        public void OnExitButton()
        {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
        }
    }
}

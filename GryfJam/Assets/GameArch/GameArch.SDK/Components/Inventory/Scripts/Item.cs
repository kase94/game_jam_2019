﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory/Create new item")]
public class Item : ScriptableObject {

    public string Name;
    public Sprite Icon;
}

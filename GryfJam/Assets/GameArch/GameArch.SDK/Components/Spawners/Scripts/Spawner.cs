﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public float spawnSpeed = 1f;
    private float _spawnTimer = 0f;
    public int spawnSize = 2;
    public Transform[] spawnPoints;
    public GameObject spawnObject;

	// Update is called once per frame
	void FixedUpdate ()
    {
        _spawnTimer += Time.fixedDeltaTime;
        if(_spawnTimer >= spawnSpeed)
        {
            for (int i = 0; i < spawnSize; i++)
            {
                var spawn = Instantiate(spawnObject);
                spawn.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
            }
            _spawnTimer -= spawnSpeed;
        }

    }
}

﻿using UnityEngine;

public class MouseTarget : MonoBehaviour, ITarget {
    private Vector3 _mousePosition;

    public Vector3 Target
    {
        get
        {
            return Input.mousePosition;
        }
    }

    private void Update()
    {
        _mousePosition = Input.mousePosition;
    }
}

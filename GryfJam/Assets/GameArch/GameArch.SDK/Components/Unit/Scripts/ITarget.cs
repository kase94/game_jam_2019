﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITarget
{ 
    Vector3 Target { get; }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttack : MonoBehaviour
{
    [SerializeField] private Transform _shotPos;

    public Transform ShotPos
    {
        get
        {
            return _shotPos;
        }

        private set
        {
            _shotPos = value;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    public delegate void UnitEvent (Unit unit);
    public UnitEvent OnDeath;
    public UnitEvent OnDamageTaken;
    public UnitEvent OnSpawn;
    public UnitEvent OnAttack;

}

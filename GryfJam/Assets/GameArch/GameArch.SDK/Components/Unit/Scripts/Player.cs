﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameArch.Core
{
    public class Player : MonoBehaviour
    {
        public Unit unit;
        // Use this for initialization
        void Start()
        {
            unit.OnDeath += OnDeath;
            unit.OnDeath.Invoke(unit);

        }

        private void OnDeath(Unit unit)
        {
            throw new NotImplementedException();
        }

        // Update is called once per frame
        void Update()
        {

        }
        private void OnDestroy()
        {
            unit.OnDeath -= OnDeath;
        }
    }
}
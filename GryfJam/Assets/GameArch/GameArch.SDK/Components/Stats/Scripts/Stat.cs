﻿using System;
using UnityEngine;
using static UnityAcitionHelper;

[Serializable]
public class Stat
{
    public string Name;
    public bool CanBeNegative = false;

    [SerializeField] protected int _value;
    [SerializeField] protected int _maxValue;

    public IntIntHandler OnValueChange;
    public IntIntHandler OnMaxValueChange;

    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            int minValue = CanBeNegative ? int.MinValue : 0;
            int newValue = Mathf.Clamp(value, minValue, _maxValue);
            int oldValue = _value;
            _value = newValue;
            OnValueChange?.Invoke(oldValue, newValue);
        }
    }

    public int MaxValue
    {
        get
        {
            return _maxValue;
        }
        set
        {
            if (value < _value)
            {
                Value = value;
            }
            int oldValue = _maxValue;
            _maxValue = value;
            OnMaxValueChange?.Invoke(oldValue, value);
        }
    }
}

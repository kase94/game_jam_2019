﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthTest : MonoBehaviour {

    public StatComponent hpComp;

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            hpComp.stat.Value = hpComp.stat.Value - Random.Range(1 , 30);
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            hpComp.stat.Value = hpComp.stat.Value + Random.Range(1, 30);
        }
    }
}

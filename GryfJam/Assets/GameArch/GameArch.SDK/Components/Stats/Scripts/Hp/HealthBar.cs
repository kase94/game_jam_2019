﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    #region Fields
    [SerializeField]
    private StatComponent hpStat;
    [HideInInspector]
    public Stat hp;
    public Image hpBar;
    public Image hpBarBorder;

    public Sprite hpBorder;
    public Sprite highHp;
    public Sprite medHp;
    public Sprite lowlHp;

    protected int _oldHealth;
    protected int _newHealth;

    #endregion

    #region Methods

    private void Awake()
    {
        hp = hpStat.stat;
    }

    private void OnEnable()
    {
        hpBarBorder.sprite = hpBorder;
        hp.OnValueChange.AddListener(OnHpChange);
        CheckHp(hp.Value, hp.MaxValue);
        _oldHealth = _newHealth = hp.Value;
        hpBar.fillAmount = (float) hp.Value / hp.MaxValue;
    }

    private void OnDisable()
    {
        hp.OnValueChange.RemoveListener(OnHpChange);
    }

    protected void OnHpChange(int oldHp, int newHp)
    {
        _oldHealth = oldHp;
        _newHealth = newHp;
    }

    private void FixedUpdate()
    {
        if(_oldHealth != _newHealth)
        {
            _oldHealth = Mathf.FloorToInt(Mathf.Lerp(_oldHealth, _newHealth, 0.3f));
            hpBar.fillAmount = (float)_oldHealth / hp.MaxValue;
            CheckHp(_oldHealth, hp.MaxValue);         
        }
    }

    protected void CheckHp(int health, int maxHelath)
    {
        if ( (float) health / maxHelath > 0.7)
        {
            hpBar.sprite = highHp;
        }
        else if ( (float) health / maxHelath > 0.3)
        {
            hpBar.sprite = medHp;
        }
        else
        {
            hpBar.sprite = lowlHp;
        }
    }

    #endregion
}

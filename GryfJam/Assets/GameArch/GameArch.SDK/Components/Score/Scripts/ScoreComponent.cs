﻿using UnityEngine;
using static UnityAcitionHelper;

namespace Components
{
    public class ScoreComponent : MonoBehaviour
    {
        public IntIntHandler OnScoreChange;

        [SerializeField] protected int _score;

        public int Score
        {
            get
            {
                return _score;
            }
            set
            {
                int newScore = Mathf.Clamp(value, 0, int.MaxValue);
                OnScoreChange?.Invoke(_score, newScore);
                _score = newScore;
            }
        }
    }
}

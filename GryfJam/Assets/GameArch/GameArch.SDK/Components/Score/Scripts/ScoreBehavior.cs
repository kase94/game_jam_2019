﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Components
{
    public class ScoreBehavior : MonoBehaviour
    {

        #region Fields

        public ScoreComponent score;
        public TextMeshProUGUI textMesh;

        protected int _oldScore;
        protected int _newScore;

        #endregion

        #region Methods

        private void OnEnable()
        {
            score.OnScoreChange.AddListener(OnScoreChange);
            _oldScore = _newScore = score.Score;
        }

        private void OnDisable()
        {
            score.OnScoreChange.RemoveListener(OnScoreChange);
        }

        protected void OnScoreChange(int oldHp, int newHp)
        {
            _oldScore = oldHp;
            _newScore = newHp;
        }

        private void FixedUpdate()
        {
            if (_oldScore != _newScore)
            {
                _oldScore = Mathf.FloorToInt(Mathf.Lerp(_oldScore, _newScore, 0.3f));
                textMesh.text = _oldScore.ToString();
            }
        }

        #endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Components
{
    public class ScoreTest : MonoBehaviour
    {
        public ScoreComponent score;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                score.Score = score.Score - Random.Range(1, 1000);
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                score.Score = score.Score + Random.Range(1, 1000);
            }
        }
    }
}
